BEGIN {
	$::DEBUG = 'a';
	binmode STDIN, ':encoding(cp932)';
	binmode STDOUT, ':encoding(cp932)';
	binmode STDERR, ':encoding(cp932)';
	use lib 'C:/Projects/bbs_pm/pkg/lib';
};

package BBS::Handler::Test;
use utf8;
use strict;
use warnings;
use Encode qw( encode decode );
use Data::Dumper;

#use BBS;
use BBS::Handler;
use Debug;



## new()
my $h = new BBS::Handler();


## set()
$h->set( sub { $h->BBS::Handler::Test::a() } );

## get()
my $func = $h->get();

## call()
my @r = $h->call();				# a
print "\n[r]\n".Dumper(@r);

##set()
#$h->set( sub { $h->BBS::Handler::Test::b() } );
#$h->call();				# b
#print "\n";
#$h->set( $func );
#$h->call();
#print "\n";




sub a {
	print "a3";
	return 3;
}

sub b {
	print "b";
}




1;