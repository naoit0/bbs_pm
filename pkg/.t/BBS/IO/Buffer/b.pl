BEGIN {
	$::DEBUG = 'a';
	binmode STDIN, ':encoding(cp932)';
	binmode STDOUT, ':encoding(cp932)';
	binmode STDERR, ':encoding(cp932)';
	use lib 'C:/Projects/bbs_pm/pkg/lib';
};

package BBS::IO::Buffer::Test;
use utf8;
use strict;
use warnings;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;

use BBS::IO::Buffer;

# 文字をstoreするときはutf8エンコードする

my @buf;
push( @buf, "A", "B", "C", "D", "E" );								# '12345'
#push( @buf, "\x0d" );												# '<CR>'
#push( @buf, "67890" );												# '67890'
#push( @buf, "\x0d" );												# '<CR>'
#push( @buf, "ABCD". "\xe3\x81\x82" );								# 'ABCDあ'
#push( @buf, "\x0d" );												# '<CR>'
#push( @buf, "1234\xe3\x81\x825" );									# '1あ2345'
#push( @buf, "\x08\x08\x08");										# '<BS><BS><BS>'
#push( @buf, "\xEF\xBD\xBB" );										# 'ｻ'
#push( @buf, "\x08\x08");											# '<BS><BS>'
#push( @buf, "1\xef\xbc\xb22" );										# '1Ｒ2' 
#push( @buf, "5\xe3". "\x81\x82" );									# '5あ'
#push( @buf, "134". "\xEF\xBD\xBB\xEF". "\xBE". "\x9D");				# '134ｻﾝ'
#push( @buf, "\x08\x08\x08\x08\x08\x08");							# '<BS><BS><BS><BS><BS><BS>'
#push( @buf, "\xEF\xBE\x8C\xEF\xBE\x9F". "\xEF\xBE". "\x99" );		# 'ﾌﾟﾙ'
#push( @buf, "zzzz5" );												# 'zzzz5'
#
#push( @buf, "\x0d\x0a" );
#push( @buf, "■■■" );												# e2, 96, a0
#push( @buf, encode('utf8', "■■■") );
#
#push( @buf, "■" );								# e2, 96, a0
#push( @buf, encode('utf8', "■") );				# e2, 96, a0
#push( @buf, "\xe2\x96\xa0" );						# '■'
#
#push( @buf, "\xe2\x96" );
#push( @buf, "\xa0" );
#
#push( @buf, "abc" );
#push( @buf, encode('utf8', "abc" ));
#
#push( @buf, "\xe3");
#push( @buf, "\x81\x82" );
#push( @buf, "\xe3\x81\x82" );
#
#push( @buf, encode('utf8', "abあc2"));
#push( @buf, "abあc2");
#
#push( @buf, encode('utf8', "えええ"));
#push( @buf, encode('utf8', "c"));

#=====================================================

## new()
my $b = new BBS::IO::Buffer;
my $b2 = new BBS::IO::Buffer;


## store()
# 'ABCDE'
map {
	$b->store( $_ );
} @buf;
print "\n[store]\n".Dumper($b);


## rstore()
# 'EDBCA'
# map {
# 	$b->rstore( $_ );
# } @buf;
# print "\n[rstore]\n".Dumper($b);


## get()
printf "\n** get=[%s]", $b->get();


## fetch()
# 'ABCDE'
# printf "\nlen=(%d) ", $b->len();
# while ( $b->len() ) {
# 	my $chr = $b->rfetch();
# 	printf "\n\tlen=(%d), chr=[%s] ", $b->len(), decode('utf8', $chr);
# }
# printf "\nlen=(%d) ", $b->len();


## rfetch()
# 'EDCBA'
# printf "\nlen=(%d) ", $b->len();
# while ( $b->len() ) {
# 	my $chr = $b->rfetch();
# 	printf "\n\tlen=(%d), chr=[%s] ", $b->len(), decode('utf8', $chr);
# }
# printf "\nlen=(%d) ", $b->len();


## flush()
# printf "\nlen=(%d) ", $b->len();
# $b->flush();
# printf "\nlen=(%d) ", $b->len();
# printf "\n** get=[%s]", $b->get();

1;