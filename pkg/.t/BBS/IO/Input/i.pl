BEGIN {
	$::DEBUG = '!';
	binmode STDIN, ':encoding(cp932)';
	binmode STDOUT, ':encoding(cp932)';
	binmode STDERR, ':encoding(cp932)';
	use lib 'C:/Projects/bbs_pm/pkg/lib';
};

package BBS::IO::Input::Test;
use utf8;
use strict;
use warnings;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;

# use BBS;
use BBS::IO;

my $bbs = new BBS;

$bbs->{'cnt'} = 0;

# 受信データ（通信データはバイナリなので、ASCII以外は\xnnで指定）
my @recv;
	# push( @recv, "12345" );											# '12345'
	# push( @recv, "\x0d" );											# '<CR>'
	# push( @recv, "67890" );											# '67890'
	# push( @recv, "\x0d" );											# '<CR>'
	# push( @recv, "ABCD". "\xe3\x81\x82" );							# 'ABCDあ'
	# push( @recv, "\x0d" );											# '<CR>'
	push( @recv, "1234\xe3\x81\x825" );									# '1あ2345'
	# push( @recv, encode('utf8', "あ") );								# 'あ'
																		# Inputで取り込むときはエンコードしてバイナリにする
																		# Outputで取り込むときはデコードして文字単位にする 
	push( @recv, "\x08\x08\x08");										# '<BS><BS><BS>'
	# push( @recv, "\xEF\xBD\xBB" );										# 'ｻ'
	# push( @recv, "\x08\x08");											# '<BS><BS>'
	# push( @recv, "1\xef\xbc\xb22" );									# '1Ｒ2' 
	# push( @recv, "5\xe3", "\x81\x82" );									# '5あ'
	# push( @recv, "134", "\xEF\xBD\xBB\xEF", "\xBE", "\x9D");			# '134ｻﾝ'
	# push( @recv, "\x08\x08\x08\x08\x08\x08");							# '<BS><BS><BS><BS><BS><BS>'
	# push( @recv, "\xEF\xBE\x8C\xEF\xBE\x9F", "\xEF\xBE", "\x99" );		# 'ﾌﾟﾙ'
	# push( @recv, "zzzz5" );												# 'zzzz5'


## new()
my $in = new BBS::IO::Input();							# 入力制御モジュール


## setting
$in->{'echo'}				= 1;						# エコーバック
$in->{'mode'}				= 1;						# ライン入力モード
# $in->{'input_size'}		= 100;						# 入力バッファは20文字
$in->{'input_size'}			= 6;						# 入力バッファ無効
$in->{'feedback'}			= 1;						# フィードバック処理	
$in->{'autocr'}				= 0;						# 自動確定
$in->{'mask'}				= 0;						# マスク入力
$in->{'maskchr'}			= '*';						# マスク文字
$in->{'cr_chr'}				= '\x0d';					# CRのみ(デフォルト)
$in->{'reject_chr'}			= '';						# 全ての制御文字を取り込まない(デフォルト)


## store()

map { $in->store($_) } @recv;


## keyin()

until ( $in->{'input_len'} < 0 ) {
	my $r = $in->keyin();
	printf "\n** input.len=(%d) ", $in->len();
	printf "\n[input.get]\n%s", Debug::dump( decode('utf8', $in->{'__Input'}->get() ) );
	printf "\n[recv.get]\n%s", Debug::dump( $in->{'__Recv'}->get() );
	printf "\n** r=[%s] ", sprintf "%02x", ord($r);			# inkey.r
	print "\n";
	my $i = <STDIN>;
}

1;