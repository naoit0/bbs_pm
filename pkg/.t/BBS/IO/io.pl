## io test server ##

BEGIN {
	$::DEBUG = '!';
	binmode STDIN, ':encoding(cp932)';
	binmode STDOUT, ':encoding(cp932)';
	binmode STDERR, ':encoding(cp932)';
	use lib 'C:/Projects/bbs_pm/pkg/lib';
	use lib 'C:/Projects/big-model/pkg/lib';
};

package BBS::IO::Test;
#===========================================================
use utf8;
use strict;
use warnings;
use Encode qw( encode decode );
use Data::Dumper;

use BBS;
use BBS::IO;
use BIGModel::System;					# (※要BIGModelアプリケーション)
use Debug;

my $bbs = new BBS;
	$bbs->{'__Param'} = new BIGModel::System::Param();		# パラメータ
	$bbs->{'cnt'} = 0;										# カウンタ


#-----------------------------------------------
# ■■■ appwork
#-----------------------------------------------
sub appwork {
	printf "\n(%s::appwork [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $me = $self->from();
	my $out = $self->node($me)->{'__Output'};
	my $in = $self->node($me)->{'__Input'};

	my $echoback = $in->keyin();				# 入力バッファにデータを入力（エコーバックが有効ならエコーバック文字が返る）
	my $inkey = $in->inkey();					# 入力された文字コード

	# 入力確認
	if ( $inkey ne '' ) {
		printf "\n** cnt=(%d) ", $self->{'cnt'};
		$self->{'cnt'} = 0;
	 	printf "\n** inkey=(%d)[%s] ", $in->{'inkey_len'}, decode('utf8',$inkey);			# ???? 入力値

		## ＣＲチェック
		if ( $in->iscr() == 1 ) {
			printf "\n** (CR)";
			printf "\n** str=[%s]", decode('utf8',$in->input());
			$self->from_send( "\r\n", 2 );
		}

		# オーバーフローチェック
		print "\n** (overflow) " if ( $in->isof() == 1 );

		# エコーバック
		my $ebstr = decode( 'utf8', $echoback );
		if ( defined($ebstr) ) {
			print "\n[ebstr]".Debug::dump( $ebstr );
			$self->from_send( $ebstr, 2 );					# エコーバック(一括送出, フローなし)
		}

		# 状態確認
		my $input = $in->input();
		printf "\n** input=(%d)[%s] ", $in->{'input_len'}, decode('utf8', $input);			# ???? 入力バッファの内容
	}

	$self->{'cnt'}++;				# カウントアップ
}


#-----------------------------------------------
# ■■■ flow_control : フローコントロール処理
#
# データ送信中はappWorkハンドラの呼び出し先が本ルーチンに切り替える。
# このルーチンでは次のような処理を行う。
# 
# Ctrl+S : 送出フラグをセット
# Ctrl+Q : 送出フラグをリセット
# Ctrl+C : 送信バッファをクリア
# 
# 実際のフロー制御は、Outputハンドラ処理で行われる。
#-----------------------------------------------
sub flow_control {
	printf "\n\n(%s::flow_control [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /!/);		# ????
	my $self = shift;
	# my $me = $self->from();
	# return if ( ( defined( $self->node($me)->{'FlowCtrl'} ) == 1 ) && ( $self->node($me)->{'FlowCtrl'} == 0 ) );			# フロー制御が無効(undef)
	
	# my $in = $self->node($me)->{'__Input'};
	# $in->keyin();
	# my $inkey = $in->inkey();
	# return unless ( defined($inkey) );							# 入力なし
	# Debug::dump($inkey);										# ????

	# if ( $inkey eq "\x03" ) {									# Ctrl+C
	# 	$self->node($me)->{'__Output'}->flush();					# 送信バッファをクリア
	# 	$self->node($me)->{'XOFF'} = 0;								# 送出フラグをリセット
	# 	$self->from_send("\r\n", 0);
	# }
	# elsif ( $inkey eq "\x11" ) {								# Ctrl+Q
	# 	$self->node($me)->{'XOFF'} = 0;								# 送出フラグをリセット
	# }
	# elsif ( $inkey eq "\x13" ) {								# Ctrl+S
	# 	$self->node($me)->{'XOFF'} = 1;								# 送出フラグをセット
	# }
}


#-----------------------------------------------
# ■■■ onSend : send( to, data, mode )
# 
# [引数]
#     to: 送信先ノード番号
#   data: 送信データ
#   mode: 送信モード
#         0 : 送信データを一文字ずつ分割して送信 (フロー制御なし)	プロンプト向き
#         1 : 送信データを一文字ずつ分割して送信 (フロー制御あり)	コンテンツ向き
#         2 : 送信データを一括送信               (フロー制御なし)	エコーバック向き
# 
# データを取得すると送信タスク(SendTask)に取り込まれる。
# 送信先ノードの切断フラグがセットされている場合はデータの取り込みは行わない。
#-----------------------------------------------
sub onsend {
	printf "\n\n(%s::onsend [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $to   = shift;			# 送信先ノード
	my $data = shift;			# 送信データ
	my $mode = shift;			# 送信モード
		$mode = ( ( defined($mode) == 1 ) && ( $mode =~ /^\d+$/) ) ? $mode : 0;

	# printf "\n** to=(%d), mode=(%d) ", $to, $mode;								# ????
	return if ( $self->node($to)->{'disconnected'} );								# 送信先ノードが切断しているなら受け入れない
	push( @{ $self->node($to)->{'SendTask'} }, [ $data, $mode ] );					# 送信データを送信タスクに保存
	# print "\n[SendTask]\n".Dumper( $self->node($to)->{'SendTask'} );				# ????
}


#-----------------------------------------------
# ■■■ Output
#-----------------------------------------------
sub output {
	printf "\n\n(%s::output [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $me = $self->from();
	# printf "\n** me=(%d) ", $me;								# ????

	my $out = $self->node($me)->{'__Output'};					# 出力制御(送信バッファ)
	my $sendtask = $self->node($me)->{'SendTask'};				# 送信タスク
	my $trap = $self->node($me)->{'AppWork-Trap'};				# AppWorkハンドラトラップ

	# print "\n[SendTask]\n".Dumper( $sendtask );				# ????
	# print "\n[trap]\n".Dumper( $trap );						# ????

	my $out_len = $out->len();									# 送信バッファのデータ長
	# printf "\n** out.len=(%d) ", $out_len;					# ????

	if ( $out_len <= 0 ) {																	# ◆ 送信バッファが空
		if ( $#{ $sendtask } < 0 ) {															# ◆ 送信タスクが空
			# print "\n** (no_task) _______________";													# ????
			# AppWorkハンドラのリセット
			if ( defined($trap) ) {																	# ◆ AppWorkハンドラトラップが残っている
				$self->setapphandler( $trap );															# AppWorkハンドラトラップをハンドラに設定して
				delete( $self->node($me)->{'AppWork-Trap'} );											# AppWorkハンドラトラップを削除
			}
		}
		else {																					# ◆ 送信タスクにデータあり
			#================================
			# ● 送信ジョブ設定
			#================================
			# print "\n** (set_job) _______________";													# ????
			my ( $data, $mode, $func ) = @{ shift( @{ $sendtask } ) };								# 送信タスクからデータを取り出す
			# printf "\n** mode=(%d), data=[%s] ", $mode, $data;										# ????

			# (1) 送信モード設定
			my @datas;
			if ( $mode == 2 ) {																		# モード２：データを一括送出
				@datas = ( $data );																		# データ全てを送信バッファに取り込む
			}
			else {																					# モード０・１：データを１文字ずつ分割して送出
				until ( $data eq '' ) {
					$data =~ s/([^\x00-\x1f]+?|[\x00-\x1f]+\w|[\x00-\x1f]*)//;								# 制御文字が含まれていたら文字の前後に結合する
					my $chr = $1;
					if ( ($chr ne '') || (defined($chr) == 1) ) {
						# printf "\n** chr=[%s] ", $chr;															# ????
						push( @datas, $chr );																	# 1文字ずつ送信バッファに取り込む
					}
				}
			}
			$out->store( @datas );																	# 送信バッファにデータを取り込む
			# print "\n[datas]"; map { print Debug::dump( $_ )."\n" } @datas;							# ????

			# (2) AppWorkハンドラセット
			unless ( defined($trap) ) {																# ◆ AppWorkハンドラトラップが未設定
				$self->node($me)->{'AppWork-Trap'} = $self->node($me)->{'AppWork'}->get();				# AppWorkに設定されているルーチンを保存
				if ( $mode == 1 ) {																		# モード１：フロー制御有効
					$self->setapphandler( sub { $self->BBS::IO::Test::flow_control(@_) } );					# フロー制御ルーチンを設定
				}
				else {																					# モード０・２：フロー制御無効
					$self->setapphandler( sub { return } );													# ダミールーチンを設定
				}
			}
		}																						# ジョブ設定ここまで
	}

	$out_len = $out->len();
	if ( $out->len() <= 0 ) {															# ◆ 送信バッファが空(undef)
		# print "\n** (no_buffer) _______________";											# ????
		return;
	}

	#================================
	# ● 送出処理
	#================================
	my $senddata;
	# print "\n** (output_data) _______________";											# ????
	my $t = $self->node($me)->{'__T-Send'};												# 送信タイマー
	if ( $t->timeout() ) {																# ◆ タイムアウトしている
		# print "\n** (timeout) _______________";												# ????
		$senddata = $out->fetch();															# 送信バッファからデータを取り出す
		if ( defined($senddata) ) {															# ◆ データをつかんでいる
			$senddata = encode( 'utf8', $senddata );
			my $interval = $self->node($me)->{'__Param'}->get('SEND_INTERVAL');					# インターバル値を取得
			# printf "\n** interval=(%d) ", $interval;											# ????
			$t->setinterval($interval) if ( defined($interval) );								# インターバルを更新
			# print "\n[senddata]\n".Dumper( $senddata );											# ????
		}
	}
	return $senddata;																	# データを送出
}


#-----------------------------------------------
# ■■■ onRecv
#-----------------------------------------------
sub onrecv {
	printf "\n\n(%s::onrecv [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $data = shift;
	my $in = $self->from_node()->{'__Input'};
	$in->store( $data );
}


#-----------------------------------------------
# ■■■ onConnect
#-----------------------------------------------
sub onconnect {
	printf "\n\n(%s::onconnect [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $me = $self->from();

	my $sock = $self->node($me)->{'__Socket'};
	my $host = $sock->peerhost();
	my $port = $sock->peerport();
	printf "\n** connected=(%d) [%s:%s] " , $me, $host, $port;

	# ■ 入力制御
	my $in = new BBS::IO::Input();
		$in->{'echo'}			= 1;			# エコーバック
		$in->{'bel'}			= 1;			# ベル
		$in->{'mode'}			= 1;			# ライン入力
		$in->{'feedback'}		= 1;			# フィードバック
		$in->{'autocr'}			= 0;			# 自動確定
		$in->{'mask'}			= 0;			# マスク入力
		$in->{'maskchr'}		= '*';			# マスク文字
		$in->{'input_size'}		= 10;			# 入力バッファサイズ
		$in->{'cr_chr'}			= '\x0d';		# 確定パターン
		# $in->{'reject_chr'}		= '';		# 取り込み拒否パターン
	$self->node($me)->{'__Input'} = $in;

	# ■ 出力制御
	my $out = new BBS::IO::Output();
	$self->node($me)->{'__Output'} = $out;

	# ■ パラメータ
	my $p = $self->{'__Param'}->add($me);
	$self->node($me)->{'__Param'} = $p;

	# ■ タイマー
	my $t = new BBS::Timer();							
	$self->node($me)->{'__T-Send'} = $t;

	# インターバル設定
	$p->set( 'SEND_INTERVAL' => 500000 );
	# $p->set( 'SEND_INTERVAL' => 20000 );
	# $p->set( 'SEND_INTERVAL' => 10000 );
	# $p->set( 'SEND_INTERVAL' => 4000 );
	# $p->set( 'SEND_INTERVAL' => 800 );
	# $p->set( 'SEND_INTERVAL' => 500 );
	# $p->set( 'SEND_INTERVAL' => 110 );

	$self->sethandler( sub { $self->BBS::IO::Test::appwork() } );
}


#-----------------------------------------------
# ■■■ onDisconnect
#-----------------------------------------------
sub ondisconnect {
	printf "\n\n(%s::ondisconnect [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	my $me = $self->from();
	my $sock = $self->node($me)->{'__Socket'};
	my $host = $sock->peerhost();
	my $port = $sock->peerport();
	printf "\n** disconnected=(%d) [%s:%s] " , $me, $host, $port;
	$self->{'__Param'}->del($me);
}


#===========================================================
$bbs->setsyshandler('onConnect',    sub { $bbs->BBS::IO::Test::onconnect(@_) });
$bbs->setsyshandler('onRecv',       sub { $bbs->BBS::IO::Test::onrecv(@_) });
$bbs->setsyshandler('Input',        sub { $bbs->BBS::IO::Test::input(@_) });
$bbs->setsyshandler('onSend',       sub { $bbs->BBS::IO::Test::onsend(@_) });
$bbs->setsyshandler('Output',       sub { $bbs->BBS::IO::Test::output(@_) });

print "\n開始しました";
$bbs->start(8888);
print "\n終了しました";


# 文字列のダンプを表示
sub strdump {
	my $str = shift;
	return join( ' ', map { unpack( "H*", $_ ) } split( //, $str ) );
}

1;