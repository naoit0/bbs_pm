BEGIN {
	$::DEBUG = '!';
	binmode STDIN, ':encoding(cp932)';
	binmode STDOUT, ':encoding(cp932)';
	binmode STDERR, ':encoding(cp932)';
	use lib 'C:/Projects/bbs_pm/pkg/lib';
};

package BBS::Timer::Test;
use utf8;
use strict;
use warnings;
use Time::HiRes qw( gettimeofday tv_interval );
use Encode qw( encode decode );
use Data::Dumper;

use BBS::Timer;

my $interval = 1000000;

##** new()
my $t1 = new BBS::Timer();
print "\n[t1]\n".Dumper($t1);


## ** setinterval()
$t1->setinterval( $interval );
print "\n[t1]\n".Dumper($t1);


# ## ** timeout()
# my @cnt;
# for my $i (0..5) {
# 	my $c = 0;
# 	until ( $t1->timeout() ) {
# 		$c++;
# 	}
# 	$cnt[$i] = $c;
# 	printf "\n** c=(%d) ", $i;
# 	$t1->setinterval( $interval ) if ($i<2);
# }
# print "\n[cnt]\n", Dumper(@cnt);


##** sumtime()
my $tm0 = [ gettimeofday() ];								# [ 1234567890, 123456 ] (sec, msec: 1=0.000001 )
print "\n[tm0]\n".Dumper($tm0);
my $tm1 = BBS::Timer::sumtime( $tm0, $interval );				# [ 1234567890, 123456 ] (sec, msec: 1=0.000001 )
print "\n[tm1]\n".Dumper($tm1);


##** difftime()
my $tm2 = BBS::Timer::difftime( $tm1, $tm0 );				# [ 1234567890, 123456 ] (sec, msec: 1=0.000001 )
print "\n[tm2]\n".Dumper($tm2);

1;