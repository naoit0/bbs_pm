﻿BEGIN {
	$::DEBUG = 'ab';
	binmode STDIN, ':encoding(cp932)';
	binmode STDOUT, ':encoding(cp932)';
	binmode STDERR, ':encoding(cp932)';
	use lib 'C:/Projects/bbs_pm/pkg/lib';
};

package App;
use utf8;
use strict;
use warnings;
use Encode qw( encode decode );
use Data::Dumper;

use BBS;
my $bbs = new BBS;

sub onconnect {
  my $self = shift;
  printf "\n\n!1接続しました (%d)\n", $self->from();
  $self->App::info();
}

sub disconnect2 {
  my $self = shift;
  printf "\n\n!2切断しました (%d)\n", $self->from();
  $self->App::info();
}

sub disconnect3 {
  my $self = shift;
  printf "\n\n!3切断しました (%d)\n", $self->from();
  $self->App::info();
}

sub disconnect4 {
  my $self = shift;
  printf "\n\n!4切断しました (%d)\n", $self->from();
  $self->App::info();
}

sub disconnect5 {
  my $self = shift;
  printf "\n\n!5切断しました (%d)\n", $self->from();
  $self->App::info();
}

sub disconnect6 {
  my $self = shift;
  printf "\n\n!6切断しました (%d)\n", $self->from();
  $self->App::info();
}

sub disconnect7 {
  my $self = shift;
  printf "\n\n!7切断しました (%d)\n", $self->from();
  $self->App::info();
}


sub info {
  my $self = shift;
  my $me = $self->from();
  printf "\naddr=[%s]", $self->node( $me )->{peerhost};
  printf "\nport=[%s]", $self->node( $me )->{peerport};
}


#sub disconnect2 {
#  my $self = shift;
#  my @n = $self->nodes();
#  printf "\n\n!接続ノード : [ %s ]\n", join( ', ', @n );
#}


$bbs->setsyshandler('onConnect',     sub { $bbs->App::onconnect() });
$bbs->setsyshandler('onDisconnect',  sub { $bbs->App::disconnect2() });
$bbs->setsyshandler('onDisconnect',  sub { $bbs->App::disconnect3() });
$bbs->setsyshandler('onDisconnect',  sub { $bbs->App::disconnect4() });
$bbs->setsyshandler('onDisconnect',  sub { $bbs->App::disconnect5() });
$bbs->setsyshandler('onDisconnect',  sub { $bbs->App::disconnect6() });
$bbs->setsyshandler('onDisconnect',  sub { $bbs->App::disconnect7() });
print "開始しました\n";
$bbs->start(8888);
print "終了しました\n";
1;
