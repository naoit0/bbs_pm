BEGIN {
	$::DEBUG = '!';
	binmode STDIN, ':encoding(cp932)';
	binmode STDOUT, ':encoding(cp932)';
	binmode STDERR, ':encoding(cp932)';
	use lib 'C:/Projects/bbs_pm/pkg/lib';
};

package BBS::Handler::Test;
use utf8;
use strict;
use warnings;
use Encode qw( encode decode );
use Data::Dumper;

use Debug;

my $str = 'あああABCD'."\x08".'あ'."\xe3\x81\x82".'ああ';

##** dump()
print "\n[dump]\n";
print Debug::dump( $str );

1;