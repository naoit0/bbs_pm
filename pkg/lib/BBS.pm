package BBS;
our $NOLOAD = 1;
#===========================================================
# BBS.pm コアシステム
#
our $VERSION = "0001.20200226.1716";		# 新版
#===========================================================
use utf8;
use strict;
use warnings;
use IO::Select;
use IO::Socket;
use Scalar::Util;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;

use BBS::Handler;						# ハンドラ制御
use BBS::Node;							# ノード制御
use BBS::Timer;							# タイマ

#==================================================
# ●コンストラクタ
# obj = new()
#
# [引数]
#    なし
#
# [返り値]
#    obj = オブジェクト
#==================================================
sub new {
	my $class = shift;
	my $self = { @_ };
	bless($self, $class);
	$self->_init();
	return $self;
}

#==================================================
# ●属性の初期化
# _init()
# 
# [引数]
#    なし
#
# [返り値]
#    なし
#==================================================
sub _init {
	my $self = shift;

	#-------------------------------
	# ●システム変数
	#-------------------------------
		$self->{'select'}							= undef;			# セレクタ
		$self->{'from'}								= undef;			# イベントを発生したノード番号
		$self->{'sigterm'}							= 0;				# サーバ停止フラグ
		$self->{'lockfile'}							= ".lock";			# ロックファイル

	#-------------------------------
	# ●システムハンドラ(handler)
	#-------------------------------

		# サーバイベントハンドラ
		#---------------------------------
		$self->{'handler'}->{'onConnect'}			= {};				# onConnectハンドラ
		#						->{handler_name}	= sub { ... };
		# $self->{'handler'}->{'onAccess'}			= {};				# onAccessハンドラ【※廃止※】
		#						->{handler_name}	= sub { ... };
		$self->{'handler'}->{'onDisconnect'}		= {};				# onDisconnectハンドラ
		#						->{handler_name}	= sub { ... };

		# 通信関連ハンドラ
		#---------------------------------
		$self->{'handler'}->{'onSend'}				= undef;			# onSendハンドラ
		#											= sub { ... };
		$self->{'handler'}->{'onRecv'}				= undef;			# onRecvハンドラ
		#											= sub { ... };

		# サーバ処理(Serve)関連ハンドラ
		#---------------------------------
		# $self->{'handler'}->{'Input'}				= undef;			# Inputハンドラ(入力処理) 【※廃止※】
		#											= sub { ... };
		$self->{'handler'}->{'Output'}				= undef;			# Outputハンドラ(出力処理)
		#											= sub { ... };
		$self->{'handler'}->{'SysWork'}				= {};				# SysWorkハンドラ（システム処理）
		#						->{handler_id}		= sub { ... };

	#-------------------------------
	# ●ノードテーブル(node)
	#-------------------------------
		$self->{'__Node'}							= new BBS::Node;			# ノード制御

	# ※以下のパラメータ名はシステム専用です。
	# 同じパラメータ名は使用しないでください。
	# 				->node(n)
	#					->{'__Socket'}				= $sock;					# ソケットオブジェクト
	#					->{'AppWork'}				= new BBS::Handler();		# AppWorkハンドラ（アプリケーション処理）
	#					->{'Disconnected'}			= n;						# 切断イベントフラグ
}

#==================================================
# 【 ノード操作 】
#==================================================
# ●接続ノードリストを得る
# list = nodes()
#
# [引数]
#    なし
#
# [返り値]
#    list = ノード番号(データ型: 配列リファレンス)
#           [ node1, node2, ... ]
#           (要素: ノード番号, 順序: 接続順)
#
# 接続しているノードリストを取得します。
#==================================================
sub nodes {
	printf "\n(%s::nodes [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	my @r = $self->{'__Node'}->nodes();											# 接続順リストから取得
	# printf "\n** nodes=[ %s ] ", join(',', @r) if ($::DEBUG =~ /b/);			# ???? 接続リスト
	return @r;
}

#==================================================
# ●ノードテーブルのデータ領域を返す
# r = node( num )
#
# [引数]
#    num : ノード番号
#
# [返り値]
#    r =
#      (失敗) : undef
#      (成功) : データ領域
#
# ノードテーブルのデータ領域を返します。
#
# ノードテーブルはそれぞれのノードが保持するデータを保持する領域で、
# 新たなノードが接続するとノードのためのデータ領域がノードテーブルに用意されます。
# 接続中のノードが切断を試みるとノードが保持するデータ領域がノードテーブルから削除されます。
#
# ノードテーブルはハッシュ変数ですので、返り値に対して操作することで、
# データを追加、削除することができます。
#
# ノードテーブルはシステムが使用するデータも保存されるため、
# 誤ってシステムデータを削除したり書き換えたりしないよう、十分注意する必要があります。
# システムが使用するデータは、init()のノードテーブルに記されています。
#==================================================
sub node {
	# printf "\n(%s::node [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $num = shift;		# ノード番号

	return $self->{'__Node'}->node($num);
}

sub from_node {
	# printf "\n(%s::node [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $num = $self->from();					# ノード番号

	return $self->{'__Node'}->node($num);
}

#==================================================
# 【 ハンドラ操作 】
#==================================================
# ●アプリケーションハンドラ設定
# r = sethandler( func, [num] )
#
# [引数]
#    func : サブルーチン ( func = sub { ... } )
#     num : ノード番号(省略時はイベントを発動したノード番号が充てられる)
#
# [返り値]
#    r =
#      (失敗) : undef
#      (成功) : データ領域
#
# setapphandler()のエイリアスです。
#==================================================
sub sethandler {
	printf "\n(%s::sethandler [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);	# ????
	my $self = shift;
	my $func = shift;		# コールバック関数
	my $num = shift;		# ノード番号
		$num = $self->{'from'} unless defined($num);				# ノード番号が未定義なら応対中のノード番号を充てる

	return $self->setapphandler( $func, $num );
}

#==================================================
# ●アプリケーションハンドラ設定
# r = setapphandler( func, [num] )
#
# [引数]
#    func : サブルーチン ( func = sub { ... } )
#     num : ノード番号(省略時はイベントを発動したノード番号が充てられる)
#
# [返り値]
#    r =
#      (失敗) : undef
#      (成功) : 1
#
# アプリケーションハンドラはアプリケーション処理を行うためのハンドラで、
# 各ノードに１つずつ所有します。
# このハンドラはユーザの要求に応答するアプリケーション処理が設定されます。
# 設定されたルーチンはサーバ処理(serve)のアプリケーション処理の段階で呼び出しが行われます。
#==================================================
sub setapphandler {
	printf "\n(%s::sethandler [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);	# ????
	my $self = shift;
	my $func = shift;
	my $num = shift;
		$num = $self->{'from'} unless defined($num);				# ノード番号が未定義なら応対中のノード番号を充てる

	return unless ( ref($func) eq 'CODE' );							# ルーチンが未定義(undef)
	return unless ( defined( $self->node($num) ) );					# ノードが開いていない(undef)

	printf "\n** num=(%d) ", $num if ($::DEBUG =~ /b/);				# ???? ノード番号
	$self->node($num)->{'AppWork'}->set($func);						# ハンドラにルーチンを設定
	return 1;
}

#==================================================
# ●システムハンドラ設定
# r = setsyshandler( handler_name, func )
#
# [引数]
#    handler_name : ハンドラ名
#            func : ハンドラルーチン ( func = sub { ... } )
#
# [返り値]
#    r =
#      (失敗) : undef
#      (成功) : 1
#
# システムハンドラは、サーバ（システム）側が持ちます。
#
# onSend, onRecv, Output の各ハンドラは、設定できるルーチンが
# それぞれ１つしか設定できませんので、ルーチンが既に設定されているところで、
# 新たなルーチンを設定すると前のルーチンは解除されます。
#
# onConnect, onDisconnect, SysWork の各ハンドラは、
# 一つのハンドラに複数のルーチンを設定できますが、ルーチンの呼び出し順序はランダムで決定しているので、
# 呼び出し順序に影響するような処理を行うと誤動作を起こす原因になります。
#
# ハンドラに設定したルーチンは削除することはできません。
# １つのルーチンしか設定できないハンドラでも、上書き設定は可能ですが、ハンドラから削除することはできません。
#
# ※ハンドラの仕組みについては、プロジェクトの解説ページをご覧ください。
#==================================================
sub setsyshandler {
	printf "\n(%s::setsyshandler [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $name = shift;		# ハンドラ名
	my $func = shift;		# ハンドラルーチン

	   if ( $name =~ /onconnect/i )    { $name = 'onConnect';    }
	elsif ( $name =~ /onaccess/i )     { $name = 'onAccess';     }
	elsif ( $name =~ /ondisconnect/i ) { $name = 'onDisconnect'; }
	elsif ( $name =~ /onsend/i )       { $name = 'onSend';       }
	elsif ( $name =~ /onrecv/i )       { $name = 'onRecv';       }
	elsif ( $name =~ /input/i )        { $name = 'Input';        }
	elsif ( $name =~ /output/i )       { $name = 'Output';       }
	elsif ( $name =~ /syswork/i )      { $name = 'SysWork';      }
	else                               { $name = undef;          }

	return if ( (defined($name) == 0) || ($name eq '') );									# ハンドラ名が未定義またはヌル(undef)
	return unless ( ref($func) eq 'CODE' );													# ルーチンが未定義(undef)

	my $hid = Scalar::Util::refaddr( $func );												# ルーチンのアドレス(=ID)を取得
	# printf "\n** name=[%s] ", defined($name) ? $name : 'undef' if ($::DEBUG =~ /b/);		# ???? ハンドラ名
	# printf "handler_id=[%s]", defined($hid) ? $hid : 'undef' if ($::DEBUG =~ /b/);			# ???? ルーチンアドレス

	# Input, Output, onRecv, onSend
	if ($name =~ /(Input|Output|onRecv|onSend)/ ) {											# ルーチンを１つだけ設定できるハンドラ
		$self->{'handler'}->{$name} = new BBS::Handler($func);								# ルーチンを新たに設定
	}
	# onConnect, onAccess, onDisconnect, SysWork
	else {																					# 多数のルーチンを設定できるハンドラ
		$self->{'handler'}->{$name}->{$hid} = new BBS::Handler($func);						# ルーチンを新たに設定
	}
}

#==================================================
# ●モジュールに定義されているローダルーチンを呼び出す
# _load()
#
# [引数]
#    なし
#
# [返り値]
#    なし
#
# ※この関数は、start()から呼び出されるためもので、個別に呼び出す必要はありません。
#
# ロードしたモジュール(パッケージ内)に
# ローダルーチン( load() )が定義されているものを全て呼び出します。
#
# ローダルーチンはモジュール(パッケージ)の初期設定を行うための専用ルーチンで、
# サーバの開始直前で呼び出しを行いますが、
# モジュール(パッケージ内)に $NOLOAD=1 があると、ローダルーチンの呼び出しを行いません。
#
# ●モジュール作成の注意点
#
# ローダルーチンはパッケージ名とファイル名(パス)を元に呼び出しを行うため、
# モジュールを配置するファイル名(パス)とパッケージ名をそろえておいてください。
#==================================================
sub _load {
	printf "\n(%s::_load [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);			# ????
	my $self = shift;

	my ($pkg) = caller;
	if ($pkg ne 'BBS') {
		print encode( 'cp932', "(error): _load()はBBSモジュール以外から呼び出すことはできません.\n");
		exit(0);
	}

	# システムライブラリパスを取得
	my $syslib = $INC[ $#INC ];												# @INCの下底にセットされていると思う
		# $syslib = '/usr/local/lib';											# UNIXだったら
	# printf "\n** syslib=[%s]", $syslib;									# ????
	my @p = split( '/', $syslib );
	# print Dumper(@p);														# ????
		$syslib = sprintf( "%s/%s",$p[0],$p[1] ) if ( $p[0] =~ /\:/ );			# Windowsなら整形
	# printf "\n** syslib=[%s]", $syslib;
	# map { printf "\n\t (%d):[%s] ", $_, $INC[$_] } 0..$#INC;				# ???? インクルードパス
	# print "\n";

	foreach my $pkg (sort(keys(%INC))) {									# ???? ロードしたパッケージ
		my $path = $INC{$pkg};
		next if ( $pkg eq 'BBS.pm' );											# BBS.pm
		next if ( $pkg =~ /^BBS\// );											# パッケージ名が'BBS/'で始まる
		next if ( $path =~ /$syslib/ );											# システムライブラリに属する

		$pkg =~ s/\.pm//;														# 拡張子を除去
		$pkg =~ s/\//::/g;														# パス文字を除去

		no strict 'refs';														# 怒られるので遮断
		my $noload = ${ $pkg."::NOLOAD" };										# ローダ抑止フラグ
			$noload = ( defined($noload) ) ? $noload : 0;
		my $ver = ${ $pkg."::VERSION" };										# パッケージのバージョン番号

		next if ( $noload == 1 );												# ローダ抑止フラグが１なら次へ
		# printf "\n** pkg=[%s]", $pkg;											# ????
		# 	printf "\n\t path=[%s]", $path;										# ????
		# 	printf "\n\t ver=(%s) ", $ver if (defined($ver));					# ????
		# 	printf "\n\t noload=(%s) ", $noload;								# ????
		# print "\n";

		my $load = $pkg.'::load';
		my $r = eval{ $self->$load };													# モジュールにload()があれば呼び出す
		# printf "\n** load.r=(%d) ", $r if (defined($r));						# ???? ローダの結果
	}
	return;
}

#==================================================
# ●イベントが発生したときに応対するノード番号を返す
# num = from()
#
# [引数]
#    なし
#
# [返り値]
#    num = ノード番号
# 
# onConnect, onDisconnect, onSend, onRecv, Serveなど、
# 何らかのイベントが発生したときに、ノード番号が変数にセットされ、
# セットされたノード番号は本関数で取得することができます。
# 
# from付き関数( from_send(), from_node() )は、
# それぞれの処理を行う際、操作の対象ノードが自動的に取得されますので、
# 呼出し工数を短縮することができます。
#==================================================
sub from {
	# printf "\n(%s::from [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	#printf "\nfrom=(%d) ", $self->{'from'} if ($::DEBUG =~ /b/);								# ????
	return $self->{'from'};
}

#==================================================
# ●CTRL+Cによるプログラム停止を無効にする
# termtrap()
#
# [引数]
#    なし
#
# [返り値]
#    なし
#
# CTRL+Cによるプログラム停止を抑止します。
# 一度呼び出すと機能を取り消すことができません。
# プログラムを停止する場合は、実行するメインプログラムのパスに
# 作成されるロックファイルを削除します。
#==================================================
sub termtrap {
	printf "\n(%s::termtrap [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	$SIG{'ALRM'} = sub { return };
	$SIG{'INT'}  = sub { return };
	$SIG{'HUP'}  = sub { return };
	$SIG{'QUIT'} = sub { return };
	$SIG{'TERM'} = sub { return };
	return;
}

#==================================================
# ●サーバの開始
# start( port )
#
# [引数]
#    port : ポート番号
#
# [返り値]
#    なし
#
# 指定したポート番号でサーバを開始し、ノードからの着信を待ちます。
# ノードからの着信、通信によってイベントハンドラを呼び出します。
# 
# ポートチェックは行っていないため、
# 既に開いているポートを指定して開始することはできますが、
# 正常に動作しない可能性があります。
#==================================================
sub start {
	printf "\n(%s::start [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $port = shift;
	if ( (defined($port) != 1 ) || ($port !~ /^\d+$/) ) {			# ポート番号が無効
		print encode( 'cp932', "(error): ポート番号を指定してください.\n" );
		exit(0);
	};

	# (1) ロックファイル作成
	open(my $fh, ">$self->{'lockfile'}");
	close($fh);

	# (2) 各モジュールのローダを実行
	$self->_load();

	# (3) サーバ処理開始
	my $listen = new IO::Socket::INET(Listen => 1, LocalPort => $port);				# ソケットリスナー(3)
	$self->{'select'} = new IO::Select($listen);									# セレクタ

	$| = 1;
	while ( $self->{'sigterm'} == 0 ) {												# 終了シグナルが発生したらループを抜ける
		my @ready = $self->{'select'}->can_read(0.0001);							# 通信イベントを監視
		printf "\n** ready=(%d) ", $#ready+1 if ($::DEBUG =~ /b/);					# ???? 行列待ちに入っているソケット数

		if ( $#ready >= 0 ) {														# 通信発生
			foreach my $socket (@ready) {											# 行列待ちを捌く
				if ($socket == $listen) {											# 【 新たなクライアントが接続した(onConnect) 】
					#==============================================
					# ■■ ソケット接続イベント処理(onConnect) ■■
					#==============================================
					print "\n\n-----[ onConnect ]----- " if ($::DEBUG =~ /b/);		# ????
					$socket = $listen->accept;										# リスナーをacceptして新しいソケットを作り
					$self->{'select'}->add($socket);								# そのソケットをセレクタに追加
					my $nodeno = $socket->fileno;									# ソケットのファイル番号を得る（ファイル番号＝ノード番号）
					$self->{'from'} = $nodeno;										# 応対中ノードの番号をfrom()にセット
					printf "\n** nodeno=(%d)\n", $nodeno if ($::DEBUG =~ /b/);		# ???? ノード番号

					my $n = $self->{'__Node'};
					$n->add($nodeno);												# ノードを追加
					$n->node($nodeno)->{'__Socket'} = $socket;						# ソケットオブジェクトをノードに保存
					$n->node($nodeno)->{'peerhost'} = $socket->peerhost();			# ノードのアドレスを保存
					$n->node($nodeno)->{'peerport'} = $socket->peerport();			# ノードのポートを保存
					$n->node($nodeno)->{'AppWork'} = new BBS::Handler();			# アプリケーションハンドラをノードに保存
					$self->_onconnect();											# onConnectイベント処理へ
				}
				else {																# 【 接続するクライアントから通信が発生した(onAccess) 】
					#===========================================
					# ■■ データ通信イベント処理(onAccess) ■■
					#===========================================
					print "\n\n-----[ onAccess ]----- " if ($::DEBUG =~ /b/);		# ????
					my $nodeno = $socket->fileno;									# ソケットのファイル番号を得る（ファイル番号＝ノード番号）
					$self->{'from'} = $nodeno;										# 応対中ノードの番号をfrom()にセット
					printf "\n** nodeno=(%d)\n", $nodeno if ($::DEBUG =~ /b/);		# ???? ノード番号

					my $rcvdata;													# 受信バッファ
					$socket->recv($rcvdata, 4096);									# 受信データを取得

					if ($rcvdata eq '') {											# 受信バッファが空(通信エラー)
						$self->_ondisconnect( $nodeno );							# onDisconnectイベント処理へ
					}
					else {															# 受信バッファにデータがある
						$self->_onrecv( $rcvdata );									# onrecvイベント処理へ
						#$self->_onaccess();										# onaccessイベント処理へ【廃止】
					}
				}
				$self->_serve( $self->{'from'} );									# サーバ処理へ
			}
		}
		else {																		# 【 サーバが待機中 】
			#===================================
			# ■■ サーバ処理処理(Serve) ■■
			#===================================
			$self->_serve();														# サーバ処理へ
		}
	}																				# 停止フラグが立たなければサーバループに戻る
	return 0;																		# 停止フラグが立たてばサーバを停止
}

#==================================================
# ●サーバ処理
# _serve( nodeno )
#
# [引数]
#    nodeno : 処理を開始するノード番号(省略可)
#
# [返り値]
#    なし
#
# ※この関数は、start()から呼び出されるもので、通常は呼び出し禁止です。
#
# サーバ処理に属するハンドラを呼び出します。
# 
# この処理では、
# サーバからノードにデータを送出する出力処理(Output)、
# アプリケーションのシステム処理(SysWork)、
# アプリケーション処理(AppWork)
# の３つのハンドラが呼び出されます。
#
# ハンドラの呼び出しはノードが接続した順で行われます。
# 例えば、接続しているノード接続順が 4, 5, 7, 6 として、
# 引数を指定すると、指定するノード番号から処理を開始し、一巡するように呼び出しを行いますが、
# 引数に7が指定されているなら、7→6→4→5の順番で呼び出しが行われます。
# 引数を省略した場合は、ノードの接続順のとおり、4→5→7→6の順番で呼び出しが行われます。
#
# ※サーバ処理の仕組みについては、プロジェクトの解説ページをご覧ください。
#==================================================
sub _serve {
	# printf "\n(%s::_serve [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $nodeno = shift;				# 呼び出し開始ノード
	my @served;						# 実行済みリスト( $served[nodeno] = flag )
	my $exit = 0;					# 終了フラグ
	my $running = 0;				# 実行中ノード番号(デフォルト:0)

	$self->{'sigterm'} = 1 unless ( -e "$self->{'lockfile'}" );							# ロックファイルが消失したら終了シグナルを発生、プログラムを停止

   	my @nodes = $self->{'__Node'}->nodes();												# 接続順リスト取得
	return if ( $#nodes < 0 );															# ノードが一つも接続されていない(undef)

	if ( defined($nodeno) ) {															# ノードが指定されていたら、指定ノードの順番（順位）を取得
		my ($order) = grep { $nodes[$_] eq $nodeno } 0..$#nodes;						# [ 4, 5, 7, 6 ] ; 4 = [0] ; 5 = [1] ; 7 = [3] ; 6 = [4]
		$running = $order if ( defined($order) );											# 順位が得られたら、実行中ノード番号を更新
	}

	# ノードのハンドラを順番に呼び出す
	until ( $exit ) {																	# 終了フラグが立ったら終了
		printf "\n\n** node=(%d), order=[%d], exit=(%d) "
						, $nodes[$running], $running, $exit if ($::DEBUG =~ /b/);			# ???? 実行中のノード番号, ノードの実行順位, 終了フラグ
		$self->{'from'} = $nodes[$running];													# 実行中のノード番号をfrom()にセット

		$self->_syswork();																	# アプリケーションシステム処理(SysWork)
		$self->_output();																	# 出力処理(Output)

		$self->_appwork();																	# アプリケーション処理(AppWork)
		$self->_output();																	# 出力処理(Output)

		$served[$running] = 1;																# 実行済みリストにノード番号を追加

		# 巡回終了チェック
		$running++;																			# ノードの実行順位＋１
		$running = ( $running > $#nodes ) ? 0 : $running;									# ノードの実行順位がノード総数を超えたら０をセット
		$exit = 1 if ( defined($served[$running]) );										# 実行するノードが実行済みリストに追加されているなら終了
		print "\n" if ($::DEBUG =~ /b/);													# ????
	}																					# 【 ループの終点 】
	printf "\n** exit serve " if ($::DEBUG =~ /b/);										# ???? サーバ処理終了
}

#==================================================
# ●出力イベント処理(Output)
# _output()
#
# [引数]
#    なし
#
# [返り値]
#    なし
#
# ※この関数は、_serve()から呼び出されるためのもので、直接呼び出し禁止です。
#
# Outputハンドラからの返り値をソケットに送出します。
# 
# Outputハンドラルーチンで返り値が指定されていないと不明な返り値が返るため、
# データを送出しない場合は返り値にundefまたはヌルを指定する必要があります。
# 
# ハンドラルーチンを抜けるときは返り値の有無に関わらず、必ずreturnを呼び出してください。
#==================================================
sub _output {
	printf "\n(%s::_output [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);			# ????
	my $self = shift;

	if ( defined($self->{'handler'}->{'Output'}) ) {
		my $data = $self->{'handler'}->{'Output'}->call();						 							# Outputハンドラを呼び出す
		# print "\n[data]".Debug::dump( decode('utf8',$data) ) if ( defined($data) );						# ????
		if ( ( defined($data) == 1 ) && ( $data ne '' ) ) {													# ◆返り値にデータが含まれている
			$self->node( $self->{'from'} )->{'__Socket'}->send($data);											# データを送出
		}
		else {																								# ◆返り値にデータが含まれていない
			# print "\n** data nothing ---- ";																	# ????
		}
	}
}

#==================================================
# ●入力イベント処理(Input) 【※廃止※】
# _input()
#
# [引数]
#    なし
#
# [返り値]
#    なし
#
# ※この関数は、_serve()から呼び出されるためのもので、直接呼び出し禁止です。
#
# ※このイベントはアプリケーション処理(appwork)で行うことができるので廃止しました。
#==================================================
# sub _input {
# 	printf "\n(%s::_input [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
# 	my $self = shift;
# 	return;
# }

#==================================================
# ●システム処理イベント処理(SysWork)
# _syswork()
#
# [引数]
#    なし
#
# [返り値]
#    なし
#
# ※この関数は、_serve()から呼び出されるためのもので、直接呼び出し禁止です。
#
# アプリケーションシステムに関する処理を行います。
#==================================================
sub _syswork {
	printf "\n(%s::_syswork [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	foreach my $hid ( keys(%{ $self->{'handler'}->{'SysWork'} }) ) {
		printf "\n** hid=(%s) ", $hid if ($::DEBUG =~ /b/);												# ???? ハンドラID
		if ( exists($self->{'handler'}->{'SysWork'}->{$hid}) ) {										# ハンドラが定義されている
			print "\n-- calling " if ($::DEBUG =~ /b/);														# ????
			$self->{'handler'}->{'SysWork'}->{$hid}->call();												# SysWorkハンドラを呼び出す
		}
		else {																							# ハンドラが未定義
			print "\n--no calling" if ($::DEBUG =~ /b/);													# ????
		}
	}
}

#==================================================
# ●アプリケーション処理イベント処理(AppWork)
# _appwork()
#
# [引数]
#    なし
#
# [返り値]
#    なし
#
# ※この関数は、_serve()から呼び出されるためのもので、直接呼び出し禁止です。
#
# ノードに対するアプリケーション処理を行います。
#==================================================
sub _appwork {
	printf "\n(%s::_appwork [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $me = $self->from();

	if ( defined( $self->node( $me )->{'AppWork'} ) ) {
		$self->node( $me )->{'AppWork'}->call();					# AppWorkハンドラを呼び出す
	}
}

#==================================================
# ●接続イベント処理
# _onconnect()
#
# [引数]
#    なし
#
# [返り値]
#    なし
#
# ※この関数は、start()から呼び出されるためのもので、直接呼び出し禁止です。
#
# この関数は新たなノードがサーバに接続されたときに呼び出されます。
#==================================================
sub _onconnect {
	printf "\n(%s::_onconnect [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	# printf "\n** node=(%d) ", $self->{'from'};														# ???? イベントを発動したノード番号
	# print "\n\t**".Dumper( $self->{'handler'}->{'onConnect'} );										# ???? ハンドラの内容

	foreach my $hid ( keys( %{ $self->{'handler'}->{'onConnect'} } ) ) {
		# printf "\n** hid=(%s) ", $hid;																	# ???? ハンドラID
		if ( defined( $self->{'handler'}->{'onConnect'}->{$hid} ) ) {
			# print " calling";																					# ???? 呼び出す
			$self->{'handler'}->{'onConnect'}->{$hid}->call();													# onConnectハンドラを呼び出す
		}
		# else {								
		# 	print " no calling";																				# ???? 呼び出せなかった
		# }
	}
}

#==================================================
# ●通信イベント処理	【※廃止※】
# _onaccess()
#
# [引数]
#    なし
#
# [返り値]
#    なし
# 
# ※データ受信はonRecvに変わり、ここは不要になったので廃止しました。
#==================================================
# sub _onaccess {
# 	printf "\n(%s::_onaccess [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);			# ????
# 	my $self = shift;
# 	return;
# }

#==================================================
# ●受信イベント(onRecv)処理
# _onrecv( data )
#
# [引数]
#    data : 受信データ
#
# [返り値]
#    なし
#
# ※この関数は、start()から呼び出されるためのもので、直接呼び出し禁止です。
#
# この関数はonAccessイベントが発生、データを取得したときに呼び出されます。
# 取得した受信データは受信ハンドラ(onRecv)に渡されます。
#==================================================
sub _onrecv {
	printf "\n(%s::_onrecv [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $data = shift;				# 受信データ

	# printf "\n** node=(%d) ", $self->{'from'};						# ???? イベントを発動したノード番号
	# printf "\n** buf_recv.length=(%d) ", length( $data );				# ???? 受信バッファのデータ長
	# printf "\n** buf_recv=[%s] ", $data;								# ???? 受信データの内容

	if ( defined( $self->{'handler'}->{'onRecv'} ) ) {
		$self->{'handler'}->{'onRecv'}->call( $data );						# onRecvハンドラを呼び出す
	}
}

#==================================================
# ●切断イベント処理
# r = _ondisconnect( num )
#
# [引数]
#    num : ノード番号
#
# [返り値]
#    r =
#       1 : 正常終了
#      -1 : 引数が指定されていない
#      -2 : ノードが開かれていない
#      -3 : すでに切断イベントを通過している
# 
# ※この関数は、start()から呼び出されるためのもので、直接呼び出し禁止です。
# 
# ハンドラなどからノードの切断を行う場合は disconnect() を使用します。
# 
# 切断イベントハンドラ(onDisconnect)を呼び出し、
# ハンドラ処理が戻るとソケットが廃棄され、ノードの整理が行われます。
#==================================================
sub _ondisconnect {
	printf "\n(%s::_ondisconnect [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $num = shift;					# ノード番号

	return -1 unless defined($num);																		# 引数が指定されていない(-1)
	return -2 unless ( defined( $self->node($num) ));													# ノードが見つからない(-2)

	# onDisconnectハンドラ内からの再呼び出し防止
	my $disconnected = $self->node($num)->{'Disconnected'};												# 切断イベントフラグを参照
	return -3 if defined($disconnected);																# すでに切断イベントを通過している(-3)
	$self->node($num)->{'Disconnected'} = 1;															# 切断イベントフラグを立てる

	# onDisconnectハンドラ呼出し
	$self->{'from'} = $num;																				# 切断するノード番号をfromに設定
	$self->{'__Node'}->_delorder($num);																	# ノード接続順リストから削除
	foreach my $hid ( keys(%{ $self->{'handler'}->{'onDisconnect'} }) ) {
		# printf "\n** hid=(%s) ", $hid;																		# ???? ハンドラID
		if ( defined( $self->{'handler'}->{'onDisconnect'}->{$hid} ) ) {									# ハンドラが定義されている
			# print " ** calling";																				# ???? 呼び出す
			$self->{'handler'}->{'onDisconnect'}->{$hid}->call();												# onDisconnectハンドラを呼び出す
		}
		# else {																								# ハンドラが定義されていない
		# 	print " no calling";																				# ???? 呼び出しなし
		# }
	}

	# ソケットの切断
	my $socket = $self->node($num)->{'__Socket'};
	$socket->close;																						# ソケットを閉じる
	$self->{'select'}->remove($socket);																	# セレクタからソケットを削除
	$self->{'__Node'}->del($num);																		# ノードテーブルを廃棄
}

#==================================================
# ●切断処理
# r = disconnect( num )
#
# [引数]
#    num : ノード番号
#
# [返り値]
#    r =
#     undef : 切断イベントをすでに通過している
#        -1 : ノード番号がないか無効
#
# ログアウトやタイムアウトなど、アプリケーション処理でノードとの切断を行うときに呼び出します。
#
# この関数では、切断イベント処理(_ondisconnect)を呼び出します。
#==================================================
sub disconnect {
	printf "\n(%s::disconnect [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);	# ????
	my $self = shift;
	my $num = shift;		# ノード番号

	# onDisconnectハンドラ内からの再呼び出し防止
	return -1 if ( (defined( $num ) == 0) || ($num !~ /^\d+$/) );
	return if defined( $self->node( $num )->{'Disconnected'} );										# 切断イベントをすでに通過している(undef)
	$self->_ondisconnect( $num );																	# 切断イベント処理を呼び出す
}

#==================================================
# ●データ送信
# r = send( nodeno, data, optional_data )
#
# [引数]
#           nodeno : 送信先ノード番号
#             data : 送信データ
#    optional_data : その他データ
#
# [返り値]
#    r =
#       1 : 完了
#      -1 : 送信データがない
#      -2 : ノードが活動していない
#
# 本関数は送信するデータをデータ送信イベントハンドラ(Send)に渡します。
# 本システムは送信データのためのデータ処理は実装されておらず、処理部分はハンドラ側で行います。
# そのため、処理はハンドラに定義する必要があります。
# 
# ※送信の仕組みについては、プロジェクトの解説ページをご覧ください。
#==================================================
sub send {
	printf "\n(%s::send [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $to = shift;						# 送信先ノード
	my $data = shift;					# 送信データ

	return -1 unless ( defined($to) );												# ノード番号がない(-1)
	return -2 if ( ( defined($data) == 0 ) || ( $data eq '' ) );					# データが未定義またはヌル(-2)

	#printf "\n** to=(%d) ", $to if ($::DEBUG =~ /b/);								# ????
	#printf "\n** data=(%d)[%s] ", length($data), $data if ($::DEBUG =~ /b/);		# ????

	return unless ( defined( $self->{'handler'}->{'onSend'} ) );					# onSendハンドラが未定義(undef)
	return $self->{'handler'}->{'onSend'}->call( $to, $data, @_ );					# 引数をSendハンドラに渡す
}

#==================================================
# ●データ送信
# r = from_send( send_data, optional_data )
#
# [引数]
#        send_data : 送信データ
#    optional_data : その他データ
#
# [返り値]
#    r =
#        1 : 完了
#       -1 : 送信データがない
#       -2 : ノードが活動していない
#       -3 : ノード番号が未指定
#
# 本関数は送信関数send()を呼び出します。
#
# 送信関数は送信先を指定する必要がありますが、
# 本関数では、自動的にイベントを発動したノードを送信先として呼び出すため、
# 送信先の指定が不要です。
# ※本関数は、send( from(), $data )と同じ処理を行います。
#==================================================
sub from_send {
	printf "\n(%s::from_send [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $to = $self->{'from'};								# 応対中のノード番号を取得

	return -3 unless ( defined($to) );						# ノード番号がない(-3)
	return $self->send( $to, @_ );							# データ送信関数(send())を呼び出す
}

# printf("\n(%s) [ %s ] ", $VERSION, __PACKAGE__ ) if ( $::DEBUG =~ /a/ );

#================================================================
=pod
=encoding utf8
=head1 スクリプト名
BBS.pm - BBS.pm コアシステム
=head1 著者
naoit0 <https://www.naoit0.com/projects/bbs_pm/>
=cut
1;