package BBS::Handler;
our $NOLOAD = 1;
#===========================================================
# BBS.pm サブモジュール【 ハンドラ制御 】
#
our $VERSION = '0001.20210225.2358';		# 新版
#===========================================================
use utf8;
use strict;
use warnings;
use Data::Dumper;
use Debug;

#==================================================
# ●コンストラクタ
# obj = new( func )
#
# [引数]
#    func : サブルーチン(省略可)
#
# [返り値]
#    obj = オブジェクト
#
# 引数を省略した場合は何もしないルーチンが設定される。
#==================================================
sub new {
	#printf "\n(%s::new [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $class = shift;
	my $func = shift;						# サブルーチン
	my $self = {};
	bless($self, $class);
	$self->_init();
	$self->set($func) if (defined($func));					# 引数が指定されていたらルーチンをセットしてみる
	return $self;
}

#==================================================
# ●属性の初期化
# _init()
# 
# [引数]
#    なし
#
# [返り値]
#    なし
#==================================================
sub _init {
	my $self = shift;
	$self->{'Func'}	= sub { return };
}

#==================================================
# ●ルーチンをセット
# r = set( func )
#
# [引数]
#    func : サブルーチン
#
# [返り値]
#    r =
#    (失敗) : undef
#    (成功) : 1
#==================================================
sub set {
	#printf "\n(%s::set [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $func = shift;						# サブルーチン

	return unless defined($func);							# 引数が定義されていない(undef)
	return unless (ref($func) eq 'CODE');					# 引数がサブルーチンでない(undef)

	$self->{'Func'} = $func;

	#print "\n\t **".Dumper( $self->{'Func'} );				# ???? 内容
	return 1;
}

#==================================================
# ●ルーチンを呼び出す
# call( arg )
#
# [引数]
#    arg : 引数(データ型: 配列)
#
# [返り値]
#    なし
#
# ルーチンの返り値が指定されていないと１(数値)が返る（原因不明）。
# そのため、呼び出すルーチンの返り値は必ず指定する必要がある。
#==================================================
sub call {
	printf "\n(%s::call [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	return &{ $self->{'Func'} }(@_);						# ルーチンを実行
}

#==================================================
# ●ルーチンのエントリポイントを返す
# func = get()
#
# [引数]
#    なし
#
# [返り値]
#    r =
#    (成功) : サブルーチンのエントリポイント
#    (失敗) : undef
#==================================================
sub get {
	printf "\n(%s::get [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	return $self->{'Func'};
}

#printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ( defined($::DEBUG) && $::DEBUG >= 1 );

#================================================================
=pod
=encoding utf8
=head1 スクリプト名
Handler.pm - BBS.pm サブモジュール【 ハンドラ制御 】
=head1 著者
naoit0 <https://www.naoit0.com/projects/bbs_pm/>
=cut
1;