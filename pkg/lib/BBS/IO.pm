package BBS::IO;
our $NOLOAD = 1;
#===========================================================
# BBS.pm コアシステム
# サブモジュール 【 入出力制御ライブラリ 】
#
our $VERSION = '----.--------.----';
#===========================================================
use utf8;
use strict;
use warnings;

use BBS::IO::Input;						# 入力制御
use BBS::IO::Output;					# 出力制御

1;