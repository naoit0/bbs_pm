package BBS::IO::Buffer;
our $NOLOAD = 1;
#===========================================================
# BBS.pm コアシステム
# サブモジュール 【 汎用バッファ 】
# 
our $VERSION = "0001.20210415.2055";		# 着手
our $VERSION = "0002.20210524.0457";		# 新版

# 汎用データの管理するためのバッファを提供します。
# バッファはブロック単位で保存し、保存するデータ型は
# 文字または文字列で、それ以外のデータ型は向かない。
#===========================================================
use utf8;
use strict;
use warnings;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;

#==================================================
# ●コンストラクタ
# obj = new()
#
# [引数]
#    なし
#
# [返り値]
#    obj = オブジェクト
#==================================================
sub new {
	my $class = shift;
	my $self = { @_ };
	bless($self, $class);
	$self->_init();
	return $self;
}

#==================================================
# ●属性の初期化
# _init()
# 
# [引数]
#    なし
#
# [返り値]
#    なし
#==================================================
sub _init {
	my $self = shift;

	#【 バッファ 】
		$self->{'buffer'}				= [];
}

#==================================================
# ●バッファの内容を返す
# data = get()
# 
# [引数]
#     なし
# 
# [返り値]
#     data = バッファの内容(データ型: 文字列)
#==================================================
sub get {
	# printf "\n\n(%s::get [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	return join( '', @{ $self->{'buffer'} } );
}

#==================================================
# ●バッファの前方からデータを抜き出す
# data = fetch( len );
# 
# [引数]
#        len : 取り出すブロック数
# 
# [返り値]
#    data =
#     (失敗) : undef
#     (成功) : 抜き出したデータ
#
# 指定するブロック数がバッファに保存されているブロック数を超えた場合は
# 全てのブロックを抜き出す。
# 
# 配列操作のshift()と同じ。
#==================================================
sub fetch {
	# printf "\n\n(%s::fetch [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $len = shift;
		$len = defined($len) ? $len : 1;
	return if ( ( $len !~ /^\d+$/ ) || ( $len <= 0 ) );							# 引数が数値でないかゼロ以下(undef)
	# printf "\n** #len=(%d) ", $len;											# ????

	my $data = join( '', splice( @{ $self->{'buffer'} }, 0, $len ) );			# 前方取得

	# print Debug::dump( $data );												# ???? 取り出したデータのダンプ
	return $data;
}

#==================================================
# ●バッファの後方からデータを抜き出す
# data = rfetch( len );
# 
# [引数]
# 	     len : 取り出すブロック数
# 
# [返り値]
#    data = 
#     (失敗) : undef
#     (成功) : 抜き出したデータ
# 
# 指定するブロック数がバッファに保存されているブロック数を超えた場合は
# 全てのブロックを抜き出す。
# 
# 配列操作のpop()と同じ。
#==================================================
sub rfetch {
	# printf "\n\n(%s::rfetch [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $len = shift;
		$len = defined($len) ? $len : 1;
	return if ( ( $len !~ /^\d+$/ ) || ( $len <= 0 ) );							# 引数が数値でないかゼロ以下(undef)
	# printf "\n** arg.len=(%d) ", $len;										# ???? 引数の値

	my $data;
	# printf "\n** buffer.len=(%d) ", $self->len();								# ???? バッファのブロック数

	my @a;
	if ( $self->len() > $len ) {												# ブロック数が指定値以上（後方取得）
		$len = $len - ($len*2);
		@a = splice( @{ $self->{'buffer'} }, $len );
	}
	else {																		# ブロック数が指定値以下（全て取得）
		@a = splice( @{ $self->{'buffer'} }, 0, $len );
	}
	$data = join( '', reverse(@a) );											# 順序を反転

	# print Debug::dump( $data );												# ???? 取り出したデータのダンプ
	return $data;
}

#==================================================
# ●バッファの前方にデータを取り込む
# store( data );
# 
# [引数]
#    data : データ(データ型: 配列)
# 
# [返り値]
#    なし
#
# 配列操作のpush()と同じ。
#==================================================
sub store {
	# printf "\n\n(%s::store [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	push( @{ $self->{'buffer'} }, @_ );											# 受信バッファに取り込む
	# print Debug::dump(@_);													# ???? 取り込んだデータのダンプ
	return;
}

#==================================================
# ●バッファの後方にデータを取り込む
# rstore( data );
# 
# [引数]
#    data : データ(データ型: 配列)
# 
# [返り値]
#    なし
#
# 配列操作のunshift()と同じ。
#==================================================
sub rstore {
	# printf "\n\n(%s::rstore [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	unshift( @{ $self->{'buffer'} }, @_ );										# 受信バッファに取り込む
	# print Debug::dump(@_);													# ???? 取り込んだデータのダンプ
	return;
}

#==================================================
# ●バッファをクリアする
# flush();
# 
# [引数]
#    なし
# 
# [返り値]
#    なし
#==================================================
sub flush {
	# printf "\n\n(%s::flush [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	$self->_init();
	return;
}

#==================================================
# ●バッファに保存したブロック数を返す
# len = len()
# 
# [引数]
#    なし
# 
# [返り値]
#    len = ブロック数(0: ブロックが無い)
#==================================================
sub len {
	# printf "\n\n(%s::len [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	return ( $#{ $self->{'buffer'} } + 1 );
}

printf("\n(%s) [ %s ] ", $VERSION, __PACKAGE__ ) if ( $::DEBUG =~ /a/ );

#================================================================
=pod
=encoding utf8
=head1 スクリプト名
Buffer.pm - BBS.pm サブモジュール 【 汎用バッファ 】
=head1 著者
naoit0 <https://www.naoit0.com/projects/bbs_pm/>
=cut
1;