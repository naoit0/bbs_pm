package BBS::IO::Input;
our $NOLOAD = 1;
#===========================================================
# BBS.pm コアシステム
# サブモジュール 【 入力制御 】
# 
our $VERSION = "0001.20210306.1722";		# 最新
our $VERSION = "0001.20210503.2346";		# 改良版

# 本モジュールはBBS.pmから受け取った受信データ（バイナリ）を
# フィルタリングして文字列などのデータに変換し、入力バッファに保存する。
#
# モジュールで扱うデータはＵＴＦ８のため、
# モジュールに入力する前にＵＴＦ８で変換しておく必要がある。
# 
# パラメータ設定関数は保留中
#===========================================================
use utf8;
use strict;
use warnings;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;
use BBS::IO::Buffer;

#==================================================
# ●コンストラクタ
# obj = new()
#
# [引数]
#    なし
#
# [返り値]
#    obj = オブジェクト
#==================================================
sub new {
	my $class = shift;
	my $self = { @_ };
	bless($self, $class);
	$self->_init();
	return $self;
}

#==================================================
# ●属性の初期化
# _init()
# 
# [引数]
#    なし
#
# [返り値]
#    なし
#==================================================
sub _init {
	my $self = shift;

	#【 バッファ 】

		# == {'__Recv'}
		# 受信バッファ
		$self->{'__Recv'}			= new BBS::IO::Buffer;

		# == {'__Input'}
		# 受信バッファ
		$self->{'__Input'}			= new BBS::IO::Buffer;

		# == {'inkey'}
		# 直前に入力した文字
		$self->{'inkey'}			= '';

		# == {'cr_check'}
		# 改行判定バッファ
		$self->{'cr_check'}			= '';

	#【 ステータス 】

		# == {'inkey_len'}
		# 直前に入力した文字のＣＰ９３２データ長
		$self->{'inkey_len'}		= 0;

		# == {'input_len'}
		# 入力バッファのＣＰ９３２データ長
		$self->{'input_len'}		= 0;

		# == {'overflow'}
		# 入力バッファのオーバーフローフラグ
		$self->{'overflow'}			= 0;

		# == {'cr'})
		# 確定フラグ
		# ＣＲが入力されるとフラグがセットされる。
		# フラグがセットされた後、keyin()を呼び出すとフラグがリセットし、入力バッファがクリアされる。
		$self->{'cr'}				= 0;

	#【 パラメータ 】

		# == {'mode'}
		# 入力モード( 1: ラインモード, 2: ブロックモード )
		$self->{'mode'}				= 1;

	#【 ラインモード用パラメータ 】

		# == {'input_size'}
		# 入力バッファに格納できる文字数(半角文字数)
		# 0を指定すると入力バッファに取り込まない。
		$self->{'input_size'}		= 80;

		# == {'feedback'}
		# 入力フィードバック処理( 0: 無効, 1: 有効 ) (※ラインモード有効時)
		# 入力フィードバックが有効の場合、ＢＳ/ＤＥＬは入力バッファに取り込まれない。
		$self->{'feedback'}			= 0;

		# == {'echo'}
		# エコーバックモード( 0: 無効, 1: 有効 )
		$self->{'echo'}				= 1;

		# == {'bel'}
		# ベルフラグ（エコーバック有効時）
		# エラー時にベルを鳴らす。
		$self->{'bel'}				= 0;

		# == {'mask'}
		# マスク処理( 0: 無効, 1:有効 ) (※エコーバック有効時)
		# マスク有効時、入力文字をマスク文字に置換する。
		$self->{'mask'}				= 0;
															
		# == {'maskchr'}
		# マスク文字(※マスク処理有効時)
		# 半角記号(1文字)を指定。
		$self->{'maskchr'}			= '*';

		# == {'autocr'}
		# 自動確定モード( 0: 無効, 1: 有効 )
		# 入力バッファがデータで満たされたときに自動的に確定する。
		$self->{'autocr'}			= 0;

		# == {'cr_chr'}
		# 入力バッファの文字列を確定するため文字(パターンで指定)
		# 通常はＣＲ(\x0d)が入力されたら確定とするが、
		# 指定した文字パターンが文字列に含まれていたら確定とする。
		# 指定できる文字は制御文字のみで、アスキー文字を指定すると無効。
		$self->{'cr_chr'}			= '\x0d';						# CRのみ(デフォルト)
		#$self->{'cr_chr'}			= '\x0d\x0a';					# CRLF

		# == {'reject_chr'}
		# 入力バッファの取り込みを拒否する文字(パターンで指定)
		# デフォルトでは全ての制御文字を入力バッファに取り込まない。
		# エスケープシーケンス装飾を行う場合は、ＥＳＣ(\x1b)を許可する。
		$self->{'reject_chr'}		= '[\x00-\x1f]';				# 全ての制御文字を取り込まない(デフォルト)
		# $self->{'reject_chr'}		= '[\x00-\x1a][\x1c-\x1f]';		# ESCを取り込む

	#【 その他 】

		# 制御文字置換テーブル(テスト用)
		$self->{'ctrl-code'}		= [
			'NUL', 'SOH', 'STX', 'ETX', 'EOT', 'ENQ', 'ACK', 'BEL',
			'BS',  'HT',  'LF',  'VT',  'FF',  'CR',  'SO',  'SI',
			'DLE', 'DC1', 'DC2', 'DC3', 'DC4', 'NAK', 'SYN', 'ETB',
			'CAN', 'EM',  'SUB', 'ESC', 'FS',  'GS',  'RS',  'US'
		];
}

#==================================================
# ●受信バッファからデータを抜き出す
# echo = keyin()
# 
# [引数]
# 	 なし
# 
# [返り値]
#    echo =
#    (データあり) : エコーバック文字列(エコーバック有効時)
#    (データなし) : undef
# 
# 【ライン入力モード(テキストデータ)】
# 抜き出したデータは入力バッファに取り込まれます。
# エコーバック有効時はエコーバック文字を返します。
# 
# 【ブロック入力モード(バイナリデータ)】
# ※現在、未対応
# バイナリデータはエコーバックされません。
#==================================================
sub keyin {
	printf "\n\n(%s::keyin [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $echo;		# エコーバック文字列

	if ( $self->{'mode'} eq 'block' )  {
		# printf "\n\n** (block-mode) _____________";						# ????
		#====================================================
		# 【 ■■■■■■ ブロックモード ■■■■■■ 】
		#====================================================

		# ここでは、受信バッファから、{'block_datasize'}文字分になるまで取り込む。
		# データを取り出して、入力バッファに取り込ませる。
	}

	else {																						# 【 ラインモード始まり 】
		# printf "\n\n** (line-mode) _____________";						# ????
		#====================================================
		# 【 ■■■■■■ ラインモード ■■■■■■ 】
		#
		# 受信バッファから１文字を取り出して入力バッファに移す。
		# 
		# 入力フィードバックが有効の場合、
		# 受信バッファから取り出したデータが制御文字(<BS>, <DEL>, <CR>)なら、入力バッファの操作を行う。
		# 
		# 入力バッファ{'input'}への取り込みを試みて、入力バッファがオーバーフローする場合は
		# 取り込みは行わず、取り出した文字は廃棄する。
		#
		# ＵＴＦ８では全角でのデータ長が２～４バイトのように可変すると文字数の計算がまともにできない。
		# ＣＰ９３２は半角１バイト、全角２バイトで構成していて、コンソールに出力する文字としても
		# データとしての文字も同様に管理できるので
		# 文字列データの管理はＵＴＦ８で行い、文字数はＣＰ９３２で算出する。
		#====================================================
		my $echo;							# エコーバック文字列
		my $exit = 0;						# == 処理終了フラグ
		$self->{'inkey'} = '';				# 入力文字のバッファをクリア
		$self->{'inkey_len'} = 0;			# 入力文字のデータ長をリセット

		# __________【 確定処理 】__________

		if ( $self->{'input_size'} > 0 ) {												# ◆入力バッファが有効
			# printf "\n** iscr=(%d) ", $self->{'cr'};										# ???? 確定フラグ
			if ( $self->{'cr'} == 1 ) {														# ◆確定フラグがセットされている
				# print "(keyin/clear) _____________";										# ????
				$self->clear();																# 入力バッファをクリア
			}

			if ( $self->{'autocr'} == 1 ) {												# ◆自動確定が有効
				if ( $self->{'input_size'} == $self->{'input_len'} ) {						# 入力バッファが埋まっていたら
					# print "\n\n** (keyin/autocr) _____________";								# ???? 自動確定
					$self->{'cr'} = 1;															# 確定フラグをセット
					$exit = 1;																	# ここで終了
				}
			}
		}

		if ( $exit == 0 ) {

			# __________【 受信バッファからデータを取得 】__________

			my $inkey = $self->getchar();												# 受信バッファから一文字取得
			return if ( ( defined($inkey) == 0 ) || ( $inkey eq '' ) );					# 受信データを取得できない(undef)
			my $inkey_len = cp932len( $inkey );											# 取得文字のデータ長
							# printf "\n** inkey.len=(%d) ",$inkey_len; 				# ????
			my $inp = $self->{'__Input'};												# 入力バッファ
			my $input_len = $self->{'input_len'};										# 入力バッファのデータ長

			# ■ 状態確認
			#--------------------------------------------------------------------------
			# print "\n[before]\n";
			# $self->_input_status();
			# print "\n";
			# printf "\n\t** inkey_len=(%s) ", $inkey_len;										# ???? 入力文字のＣＰ９３２データ長
			# printf "\n\t** inkey=[%s] ", ( $inkey =~ /[\x00-\x1f]/ )							# ???? 入力文字
			# 							? '<'.$self->{'ctrl-code'}->[ ord($inkey) ].'>'
			# 							: $inkey;
			# print "\n";
			#--------------------------------------------------------------------------

			# __________【 ここから取り込み処理 】__________

			if ( $self->{'input_size'} > 0 ) {											# ◆入力バッファが有効

				#------------------------------------------
				# ●確定チェック(CR)
				#------------------------------------------
				$self->{'cr_check'} .= $inkey;												# 改行判定バッファに入力データを追加
				$self->{'cr_check'} = substr( $self->{'cr_check'}, -10 );					# 後方10バイトを残す
					# printf "\n\t** cr_check=[ %s ] "
					# 	, join( ' ', map { unpack( "H2", $_ ) } split( //, encode('utf8', $self->{'cr_check'} ) ) );		# ???? 改行判定バッファのダンプ

				my $cr_chr = $self->{'cr_chr'};
				if ( $self->{'cr_check'} =~ /$cr_chr/ ) {									# ◆改行パターンが入力された
					# print "\n\n** (keyin/carridge_return) __________";							# ???? 
					$self->{'cr'} = 1;															# 確定フラグをセット
					# $echo = "\x0d" if ( $self->{'echo'} == 1 );									# CRをセット（エコーバックが有効なら）
					$exit = 1;																	# ここで処理終了
				}

				# __________【 ここからフィードバック処理 】__________
				# ◆入力フィードバックが有効
				if ( $self->{'feedback'} == 1 ) {
					if ( $inkey =~ /[\x08|\x7f]/) {																# <BS>, <DEL>
						#------------------------------------------
						# 入力フィードバック
						#------------------------------------------
						# 入力文字が
						# 	<BS>		入力バッファ後退
						# 	<DEL>		入力バッファ後退
						#------------------------------------------
						if ( $input_len > 0 ) {																		# ◆入力バッファにデータあり
							# print "\n\n** (keyin/delete_chr) _____________";											# ???? 後退処理開始
							my $delchar = $inp->rfetch();																# 入力バッファから１文字除去
								# printf "\n\t** delchar=[%s] ", decode('utf8', $delchar);									# ???? 削除文字, 削除文字のＣＰ９３２データ長
							my $delchar_len = cp932len( $delchar );														# 削除文字のＣＰ９３２データ長を取得
								# printf "\n\t** delchar_len=(%d) ", $delchar_len;											# ???? 削除文字, 削除文字のＣＰ９３２データ長
							$input_len = ( $input_len - $delchar_len );													# 入力バッファのデータ長から削除した文字のデータ長だけ差し引く
							$echo = "\x08 \x08" x $delchar_len if ( $self->{'echo'} == 1 );								# 削除文字のデータ長分の後退文字列を返す（エコーバックが有効なら）
							$self->{'overflow'} = 0;																	# オーバーフローフラグをリセット
							# printf "\n** input=(%d)[%s] ", $input_len, decode('utf8', $inp->get());					# ????
						}
						else {																						# ◆入力バッファにデータなし
							# print "\n\n** (keyin/empty) _____________";													# ???? バッファが空
							$echo = "\x07" if ( ( $self->{'echo'} == 1 ) && ( $self->{'bel'} == 1 ) );					# ベルを鳴らす（エコーバックが有効なら）
						}
						$exit = 1;																					# ここで処理終了
					}
				}
				# __________【 ここまでフィードバック処理 】__________

				#------------------------------------------
				# ■ フィードバック無効
				# ■ フィードバック処理なし
				# ■ ＣＲ以外の文字
				#------------------------------------------
				# ◆処理終了フラグがセットされていない
				if ( $exit == 0 ) {
					# print "\n\n** (keyin/input) _____________";														# ???? 入力処理開始
					my $reject_chr = $self->{'reject_chr'};															# 入力文字の受け入れ拒否パターンを取得
					my $reject = ( $reject_chr eq '' )
									? 0
									: ( $inkey =~ /$reject_chr/ ) ? 1 : 0;
					# printf "\n**reject_pttn=[%s]", $self->{'reject_chr'};											# ????
					# printf "\n**reject==(%d)", $reject;															# ????

					unless ( $reject ) {																			# ◆入力文字が受け入れ拒否に該当しない
						# printf "\n\n** (keyin/accept) _____________";													# ???? 入力バッファ追加許可	
						# printf "\n** input_len=(%d) ", $input_len;							# ????
						# printf "\n** inkey_len=(%d) ", $inkey_len;							# ????
						# printf "\n** input_size=(%d) ", $self->{'input_size'};				# ????
						if ( ( $input_len + $inkey_len ) > $self->{'input_size'} ) {									# ◆入力バッファのデータ長＋入力文字のデータ長が、入力バッファサイズを超える（オーバーフロー）
							# print "\n\n** (keyin/overflow) _____________";													# ???? オーバーフロー（データは取りこぼす）
							$self->{'overflow'} = 1;																		# 入力バッファのオーバーフローフラグをリセット
							$echo = "\x07" if ( ( $self->{'echo'} == 1 ) && ( $self->{'bel'} == 1 ) );						# エコーバック有効＋ベルが有効ならならベルを鳴らす(BEL)
						}
						else {																							# ◆入力バッファのデータ長＋入力文字のデータ長が、入力バッファサイズを超えない
							# print "\n\n** (keyin/add_buffer) _____________";												# ???? 入力バッファ取り込み
							$self->{'overflow'} = 0 if ( $self->{'overflow'} == 1 );										# 入力バッファのオーバーフローフラグをリセット（フラグがセットされていたら）
							$input_len = ( $input_len + $inkey_len );														# 入力バッファのデータ長に入力文字のデータ長を加算
							$inp->store($inkey);																			# 入力文字を入力バッファに取り込む
							if ( $self->{'echo'} == 1 ) {																	# ◆ エコーバックが有効
								$echo = ( $self->{'mask'} == 1 )																# マスクが有効→入力文字のデータ長分のマスク文字を返す
														? $self->{'maskchr'} x $inkey_len : $inkey;								# マスクが無効→入力文字を返す
							}
						}
					}
					else {																							# ◆入力文字が受け入れ拒否に該当する
						# printf "\n\n** (keyin/reject) _____________";													# ???? 入力バッファ追加拒否
						$echo = "\x07" if ( ( $self->{'echo'} == 1 ) && ( $self->{'bel'} == 1 ) );						# エコーバックが有効ならベルを鳴らす(BEL)
					}
				}
			}
			else {
				# ◆エコーバックが有効
				if ( $self->{'echo'} == 1 ) {
					$echo = ( $self->{'mask'} == 1 )															# マスクが有効→入力文字のデータ長分のマスク文字を返す
											? $self->{'maskchr'} x $inkey_len : $inkey;							# マスクが無効→入力文字を返す
				}
			}
			# __________【 ここまで取り込み処理 】__________


			$self->{'input_len'} = $input_len;																# データ更新
			$self->{'inkey'} = $inkey;
			$self->{'inkey_len'} = $inkey_len;
		}

		# ■ 状態確認
		#--------------------------------------------------------------------------
		# print "\n[after]\n";
		# $self->_input_status();
		# print "\n";
		# printf "\n\t** iscr=(%d) ", $self->{'cr'};															# ???? 確定フラグ
		# printf "\n\t** define.echo=(%d) ", defined($echo);													# ???? 入力文字の定義状態
		# printf "\n\t** echo=[%s] ", ( $echo =~ /[\x00-\x1f]/ )												# ???? 入力文字
		# 												? '<'.$self->{'ctrl-code'}->[ ord($echo) ].'>'
		# 												: $echo if ( defined($echo) );
		# print "\n";
		#--------------------------------------------------------------------------
		# printf "\n** echo=[%s] ", decode('utf8', $echo) if ( defined($echo) );						# ???? エコーバック（エコーバック有効時）
		return ( $self->{'echo'} == 1 ) ? $echo : undef;
	}
}																								# 【 ラインモード終わり 】

#==================================================
# ●受信バッファから一文字取得
# char = getchar()
# 
# [引数]
#    なし
# 
# [返り値]
#    char = 
#      (成功) : 取得した文字
#      (失敗) : undef (受信バッファにデータがない)
#==================================================
sub getchar {
	# printf "\n\n(%s::getchar [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $recv = $self->{'__Recv'};

	return if ( $recv->len() <= 0 );																# 受信バッファにデータがない(undef)
	# printf "\n** recv.len=(%d) ", $recv->len();													# ???? 受信バッファのデータ長(取得前)

	my $exit = 0;						# 終了フラグ
	my $inkey = undef;					# 取り出した文字

	# 文字として識別できるまで受信バッファから取得を繰り返す
	until ( $exit > 0 ) {
		my $pick = $recv->fetch();																		# 受信バッファから１バイト取得
		# Debug::dump($pick);																			# ???? 取得データ
		$inkey .= $pick;																				# 取得データを連結

		# ＵＴＦ８文字判定（ざっくり）
		$exit = 5 if ( $recv->len() <= 0 );																# 受信バッファが空(※取得データが不完全)
		$exit = 4 if ( $inkey =~ /[\xF0-\xF4][\x80-\xBF][\x80-\xBF][\x80-\xBF]/ );						# ４バイト文字
		$exit = 3 if ( $inkey =~ /[\xE0-\xEF][\x80-\xBF][\x80-\xBF]/ );									# ３バイト文字
		$exit = 2 if ( $inkey =~ /[\xC2-\xDF][\x80-\xBF]/ );											# ２バイト文字
		$exit = 1 if ( $inkey =~ /[\x00-\x7f]/ );														# １バイト文字( 制御文字・ASCII )
	}

	# printf "\n** exit=(%d) ", $exit;																# ???? 終了フラグの値
	# printf "\n**[inkey]%s", Debug::dump( decode('utf8', $inkey) );								# ???? 取得した文字
	# printf "\n** recv.len=(%d) ", $recv->len();													# ???? 受信バッファのデータ長(取得後)
	return $inkey;																					# 取得した文字を返す
}

#==================================================
# ●入力バッファをリセット
# clear()
# 
# [引数]
#    なし
# 
# [返り値]
#    なし
#==================================================
sub clear {
	# printf "\n\n(%s::clear [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	$self->{'__Input'}->flush();			# 入力バッファをクリア
	$self->{'input_len'} = 0;				# 入力バッファのデータ長をリセット
	$self->{'cr_check'} = '';				# 制御文字バッファをクリア
	$self->{'cr'} = 0;						# 確定フラグをリセット
}

#==================================================
# ●受信バッファにデータを取り込む
# store( data )
# 
# [引数]
#    data : データ(データ型: 文字列)
# 
# [返り値]
#    なし
#==================================================
sub store {
	# printf "\n\n(%s::store [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $data = join('', @_);

	return unless ( defined($data) );						# 受信データがない(undef)

	my @datas = split( //,$data );							# １バイトずつ分割
	return $self->{'__Recv'}->store( @datas );				# 受信バッファに取り込む
}

#==================================================
# ●パラメータ設定
# r = config( 'type' => value )
# 
# [引数]
#    type  : 設定名
#    value : 設定値
# 
# [返り値]
#    r =
#      (失敗) : undef
#      (成功) : 1
# 
# ※保留中
#==================================================
# sub config {
# 	printf "\n\n(%s::config [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
# 	my $self = shift;
# 	my $type = shift;			# 設定名
# 	my $value = shift;			# 設定値

# 	$type = lc($type);
# 	if ( $type =~ /^(reject_chr|maskchr)$/i ) {										# 設定値が数値でないのもの
# 		$self->{$type} = $value;														# (設定値は無保証)
# 		return 1;																		# 設定成功(1)
# 	}
# 	elsif ( $type =~ /^(autocr|mask|echo|input_size|input_datasize)$/i ) {			# 設定名チェック（規定の名前）
# 		if ( $value =~ /\d+/ ) {														# 設定値チェック
# 			$self->{$type} = $value;
# 			return 1;																		# 設定成功(1)
# 		}
# 	}
# 	return;																			# 設定失敗(undef)
# }

#==================================================
# ●入力モード設定
# mode( mode )
# 
# [引数]
# 	 mode : モード(1: ラインモード, 2: ブロックモード)
# 
# [返り値]
#    なし
# 
# ※保留中
#==================================================
# sub mode {
# 	printf "\n\n(%s::mode [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
# 	my $self = shift;
# 	my $type = shift;			# 設定値

# 	return unless ( ( defined($type) == 0 ) || ( $type =~ /^\d+$/ ) );				# 設定値が無効(undef)
# 	$self->{'mode'} = $type;
# }

#==================================================
# ●入力バッファを返す（ラインモード有効時）
# str = input()
# 
# [引数]
#    なし
# 
# [返り値]
#    str =
#	  (失敗) : '' (ラインモードが無効)
#     (成功) : 入力バッファの文字列
#==================================================
sub input {
	printf "\n\n(%s::input [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	return unless ( $self->{'mode'} == 1 );			# ラインモードでない(undef)
	return $self->{'__Input'}->get();				# 入力バッファを返す
}

#==================================================
# ●直前に入力した文字コードを返す（ラインモード有効時）
# chr = inkey()
# 
# [引数]
#    なし
# 
# [返り値]
#    chr =
#     (失敗) : undef (ライン入力モードが無効)
#     (成功) : 文字コード
#==================================================
sub inkey {
	printf "\n\n(%s::inkey [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	return unless ( $self->{'mode'} == 1 );		# ラインモードでない(undef)
	return $self->{'inkey'};					# 入力文字を返す
}

#==================================================
# ●入力バッファの確定状態を返す（ラインモード有効時）
# r = iscr()
# 
# [引数]
#    なし
# 
# [返り値]
#    r =
#     undef : ライン入力モードが無効
#         0 : 確定していない
#         1 : 確定している
#==================================================
sub iscr {
	# printf "\n\n(%s::iscr [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	return unless ( $self->{'mode'} == 1 );		# ラインモードでない(undef)
	return $self->{'cr'};						# 確定状態を返す
}

#==================================================
# ●入力バッファのオーバーフロー状態を返す（ラインモード有効時）
# r = isof()
# 
# [引数]
#    なし
# 
# [返り値]
#    r =
#     undef : ライン入力モードが無効
#         0 : オーバーフローしていない
#         1 : オーバーフローしている
#==================================================
sub isof {
	# printf "\n\n(%s::isof [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	return unless ( $self->{'mode'} == 1 );		# ラインモードでない(undef)
	return $self->{'overflow'};					# オーバーフロー状態を返す
}

#==================================================
# ●入力バッファの文字長を返す
# len = len()
# 
# [引数]
#    なし
# 
# [返り値]
#    len = 文字長
# 
# 文字長はＣＰ９３２文字で換算した文字数を返します。
#==================================================
sub len {
	# printf "\n(%s::len [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	return $self->{'input_len'};
}

#==================================================
# ●ＵＴＦ８文字からＣＰ９３２文字のデータ長を返す
# r = cp932len( utf8str );
# 
# [引数]
#    utf8str = ＵＴＦ８文字列
# 
# [返り値]
#    r =
#      (成功) : データ長
#      (失敗) : 0
# 
# 置換性能はEncodeに委ねるが、相互置換に失敗する可能性も考えられるので、
# 相互置換を行い、前後のデータが一致したら文字長を返します。
#==================================================
sub cp932len {
	printf "\n(%s::cp932len [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $utf8_a = shift;																				# ＵＴＦ８バイナリ（元データ）
	my $cp932 = encode( "cp932", decode('utf8', $utf8_a) );											# ＣＰ９３２バイナリ（ＵＴＦ８からの変換）
	my $utf8_b = encode( "utf8", decode('cp932', $cp932) );											# ＵＴＦ８バイナリ（ＣＰ９３２からの逆変換）

	# 相互置換して前後のデータが一致するか比較
	my $ok = ( $utf8_a eq $utf8_b ) ? 1 : 0;
	# printf "\n** ok=(%d) ", $ok;																		# ???? 結果
	my $cp932len;
	unless ( $ok ) {															# ◆不一致
		print "\n[utf8_a]\n";														# ???? ＵＴＦ８バイナリ（元データ）
		map { print "$_ " } map { unpack( "H*", $_ ) } split(//, $utf8_a);
		print "\n\n[cp932]\n";														# ???? ＣＰ９３２バイナリ（ＵＴＦ８からの変換）
		map { print "$_ " } map { unpack( "H*", $_ ) } split(//, $cp932);
		print "\n\n[utf8_b]\n";														# ???? ＵＴＦ８バイナリ（ＣＰ９３２からの逆変換）
		map { print "$_ " } map { unpack( "H*", $_ ) } split(//, $utf8_b);
	}
	else {																		# ◆一致
		$cp932len = length($cp932);
		# printf "\n** cp932len=(%d) ", $cp932len;									# ???? ＣＰ９３２文字長
	}
	return ( $ok == 1 ) ? $cp932len : 0;											# 返り値（一致: ＣＰ９３２文字長, 不一致: 0）
}

sub _input_status {
	#==================================================
	# ■ 入力バッファの状態確認（デバッグ用）
	# _input_status();
	# 
	# [引数]
	# 	なし
	# 
	# [返り値]
	# 	なし
	#==================================================
	printf "\n\n(%s::_input_status [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	# printf "\n\t** input_len=(%d) ", $self->{'input_len'};														# ???? 入力バッファのデータ長（更新後）
	# printf "\n\t** input=[%s] ", $self->{'__Input'}->get();																# ???? 入力バッファ（更新後）
	# printf "\n\t** input_dump=[ %s ] "
	# 			, join( ' ', map { unpack( "H2", $_ ) } split( //, encode('utf8', $self->{'__Input'}->get() ) ) );		# ???? 入力文字のダンプ
}

printf("\n(%s) [ %s ] ", $VERSION, __PACKAGE__ ) if ( $::DEBUG =~ /a/ );

#================================================================
=pod
=encoding utf8
=head1 スクリプト名
Input.pm - BBS.pm サブモジュール 【 入力制御 】
=head1 著者
naoit0 <https://www.naoit0.com/projects/bbs_pm/>
=cut
1;