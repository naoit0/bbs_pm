package BBS::IO::Output;
our $NOLOAD = 1;
#===========================================================
# BBS.pm コアシステム
# サブモジュール 【 出力制御 】
# 
our $VERSION = "----.20210227.2211";		# 新版
our $VERSION = "----.20210420.1547";		# 改良版
#===========================================================
use utf8;
use strict;
use warnings;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;
use BBS::IO::Buffer;

#==================================================
# ●コンストラクタ
# obj = new()
#
# [引数]
#    なし
#
# [返り値]
#    obj = オブジェクト
#==================================================
sub new {
	my $class = shift;
	my $self = { @_ };
	bless($self, $class);
	$self->_init();
	return $self;
}

#==================================================
# ●属性の初期化
# _init()
# 
# [引数]
#    なし
#
# [返り値]
#    なし
#==================================================
sub _init {
	my $self = shift;

	# 送信バッファ
	$self->{'__Send'} = new BBS::IO::Buffer;
}

#==================================================
# ●送信バッファからデータを抜き出す
# data = fetch( len )
# 
# [引数]
#    len : 抜き出すデータ長(文字数)
# 
# [返り値]
#    data = 抜き出したデータ
#==================================================
sub fetch {
	printf "\n\n(%s::fetch [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $len = shift;
	return $self->{'__Send'}->fetch( $len );
}

#==================================================
# ●送信バッファにデータを取り込む
# store( data )
# 
# [引数]
#    data = データ(データ型: 配列)
# 
# [返り値]
#    なし
#==================================================
sub store {
	printf "\n\n(%s::store [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	# print "\n";										# ????
	# map { print Debug::dump( $_ )."\n" } @_;			# ????
	$self->{'__Send'}->store( @_ );						# 送信バッファに取り込む
}

#==================================================
# ●バッファをクリアする
# flush()
# 
# [引数]
#    なし
# 
# [返り値]
#    なし
#==================================================
sub flush {
	printf "\n\n(%s::flush [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	return $self->{'__Send'}->flush();
}

#==================================================
# ●バッファのデータ長を返す
# len = len()
# 
# [引数]
#    なし
# 
# [返り値]
#    len = データ長
#==================================================
sub len {
	printf "\n\n(%s::len [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	return $self->{'__Send'}->len();
}

printf("\n(%s) [ %s ] ", $VERSION, __PACKAGE__ ) if ( $::DEBUG =~ /a/ );

#================================================================
=pod
=encoding utf8
=head1 スクリプト名
Output.pm - BBS.pm サブモジュール 【 出力制御 】
=head1 著者
naoit0 <https://www.naoit0.com/projects/bbs_pm/>
=cut
1;