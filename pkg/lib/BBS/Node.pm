package BBS::Node;
our $NOLOAD = 1;
#===========================================================
# BBS.pm サブモジュール【 ノードテーブル操作 】
#
our $VERSION = '0001.20210226.0004';		# 新版
#===========================================================
use utf8;
use strict;
use warnings;
use Data::Dumper;
use Debug;

#==================================================
# ●コンストラクタ
# obj = new()
#
# [引数]
#    なし
#
# [返り値]
#    obj = オブジェクト
#==================================================
sub new {
	#printf "\n(%s::new [%s]) ", __PACKAGE__, Debug::_caller(caller);		# ????
	my $class = shift;
	my $self = {};
	bless($self, $class);
	$self->_init();
	return $self;
}

#==================================================
# ●属性の初期化
# _init()
# 
# [引数]
#    なし
#
# [返り値]
#    なし
#==================================================
sub _init {
	my $self = shift;
	$self->{'Order'} = [];		# ノード開設順
	$self->{'Node'} = {};		# ノードテーブル
}

#==================================================
# ●接続中のノードリストを得る
# list = nodes()
# 
# [引数]
#    なし
# 
# [返り値]
#    list = ノードリスト(データ型: 配列リファレンス)
#           [ num1, num2, ... ]
#           (要素: ノード番号, 順序: 接続順)
# 
# 接続しているノードのリストを取得します。
#==================================================
sub nodes {
	#printf "\n(%s::nodes [%s]) ", __PACKAGE__, Debug::_caller(caller);		# ????
	my $self = shift;

	#my @nodes = @{ $self->{'Order'} };										# ????
	#printf "\n** nodes=(%d)[ %s ]\n", $#nodes+1, join(',', @nodes);		# ???? 接続順リスト

	return @{ $self->{'Order'} };
}

#==================================================
# ●ノードテーブルのデータ領域を返す
# node( num )
#
# [引数]
#    num : ノード番号
#
# [返り値]
#    r =
#       (失敗) : undef
#       (成功) : データ領域
#
# ノードテーブルのデータ領域を返します。
#
# ノードテーブルはそれぞれのノードが保持するデータを保持する領域で、
# 新たなノードが接続するとノードのためのデータ領域がノードテーブルに用意されます。
# 接続中のノードが切断を試みるとノードが保持するデータ領域がノードテーブルから削除されます。
#
# ノードテーブルはハッシュ変数ですので、返り値に対して操作することで、
# データを追加、削除することができます。
#
# ノードテーブルはシステムが使用するデータも保存されるため、
# 誤ってシステムデータを削除したり書き換えたりしないよう、十分注意する必要があります。
# システムが使用するデータは、init()のノードテーブルに記されています。
#==================================================
sub node {
	#printf "\n(%s::node [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $num = shift;				# ノード番号

	return undef unless ( defined($num) );									# 引数がない(undef)
	return undef if ( ($num !~ /^\d+$/) || ($num < 0) );					# ノード番号が無効(undef)
	return undef unless ( defined($self->{'Node'}->{$num}) );				# ノードが開いていない(undef)

	#printf "\n** node=(%d) ", $num;										# ???? ノード番号
	#print "\n\t**".Dumper( $self->{'Node'}->{$num} );						# ???? ノード領域の内容

	return $self->{'Node'}->{$num};
}

#==================================================
# ●ノードを削除
# del( num )
#
# [引数]
#      num : ノード番号
#
# [返り値]
#    r =
#        1 : 廃棄成功
#       -1 : 引数がない
#       -1 : ノード番号が無効
#       -2 : 廃棄失敗
#
# 指定するノードのデータ領域をノードテーブルから削除します。
#==================================================
sub del {
	#printf "\n(%s::del [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);					# ????
	my $self = shift;
	my $num = shift;					# ノード番号

	#printf "\n** nodes=(%d)[ %s ]\n", $#{ $self->{'Order'} }+1, join(',', @{ $self->{'Order'} });			# ???? 接続順リスト（削除前）
	return -1 unless ( defined($num) );																		# 引数がない(-1)
	return -1 if ( ($num !~ /^\d+$/) || ($num < 0) );														# ノード番号が無効(-1)

	if ( exists($self->{'Node'}->{$num}) ) {																# ノードが割り当てられている
		#printf "\n>>> delete=(%d) ", $num;

		delete( $self->{'Node'}->{$num} );																		# ノードテーブルからノードのデータ領域を削除
		$self->_delorder( $num );																				# ノード接続順リストから削除
		return 1;																								# 削除成功(1)
	}
	else {																									# ノードが割り当てられていない
		#printf "\n>>> (no_delete) ";
		return -2;																								# 削除失敗(-2)
	}
}

#==================================================
# ●ノード接続順リストから指定するノード番号を削除
# _delorder( num )
#
# [引数]
#    num : ノード番号
#
# [返り値]
#    r =
#      -1 : ノード番号が無効
#
# 指定するノードのデータ領域をノードテーブルから削除します。
#==================================================
sub _delorder {
	# printf "\n(%s::_delorder [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);				# ????
	my $self = shift;
	my $num = shift;					# ノード番号

	return -1 unless ( defined($num) );																		# 引数がない(-1)
	return -1 if ( ($num !~ /^\d+$/) || ($num < 0) );														# ノード番号が無効(-1)

	# printf "\n** nodes=(%d)[ %s ] ", $#{ $self->{'Order'} }+1, join(',', @{ $self->{'Order'} });			# ???? 接続順リスト（削除前）
	my @neworder;
	map { push (@neworder, $_) if ( $_ != $num ) } @{ $self->{'Order'} };									# 接続順リストからノード番号を除去
	@{ $self->{'Order'} } = @neworder;
	# printf "\n** nodes=(%d)[ %s ] ", $#{ $self->{'Order'} }+1, join(',', @{ $self->{'Order'} });			# ???? 接続順リスト（削除後）
}

#==================================================
# ●ノードを追加
# add( num )
#
# [引数]
#    num = ファイル番号(必須)
#
# [返り値]
#    r =
#            -1 : (失敗) 引数がない
#            -1 : (失敗) ファイル番号が無効
#            -2 : (失敗) ノードがすでに割り当てられている
#    ノード番号 : (成功) 追加したノード番号
#
# 新たに接続したノードのデータ領域をノードテーブルに確保します。
#==================================================
sub add {
	#printf "\n(%s::add [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);					# ????
	my $self = shift;
	my $num = shift;														# ファイル番号(必須)

	#printf "\n\t** num=(%d) ", $num if ( defined($num) );					# ???? ノード番号
	#print "\n\t**".Dumper( $self->{'Node'} );								# ???? ノードテーブル（追加前）
	#print "\n";															# ????

	return -1 unless ( defined($num) );										# 引数がない(-1)
	return -1 if ( ($num !~ /^\d+$/) || ($num < 0) );						# ファイル番号が無効(-1)
	return -2 if ( exists( $self->{'Node'}->{$num} ) );						# ノードがすでに割り当てられている(-2)

	push( @{ $self->{'Order'} }, $num );									# 接続順リストにノード番号を追加
	$self->{'Node'}->{$num} = {};											# ノードテーブルにデータ領域を確保

	#print "\n\t**".Dumper( $self->{'Node'} );								# ???? ノードテーブル（追加後）
	#print "\n";															# ????

	return $num;															# ノード番号を返す
}

#printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ( defined($::DEBUG) && $::DEBUG >= 1 );

#================================================================
=pod
=encoding utf8
=head1 スクリプト名
Node.pm - BBS.pm サブモジュール【 ノードテーブル操作 】
=head1 著者
naoit0 <https://www.naoit0.com/projects/bbs_pm/>
=cut
1;