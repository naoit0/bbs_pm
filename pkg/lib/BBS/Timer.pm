package BBS::Timer;
our $NOLOAD = 1;
#===========================================================
# BBS.pm コアシステム
# サブモジュール 【 タイマ 】
#
our $VERSION = '0001.20200422.1934';		# 初版
#===========================================================
use utf8;
use strict;
use warnings;
use Time::HiRes qw( gettimeofday tv_interval );
use Data::Dumper;

#==================================================
# ●コンストラクタ
# obj = new()
#
# [引数]
#    なし
#
# [返り値]
#    obj = オブジェクト
#==================================================
sub new {
	my $class = shift;
	my $self = { @_ };
	bless($self, $class);
	$self->_init();
	return $self;
}

#==================================================
# ●属性の初期化
# _init()
# 
# [引数]
#    なし
#
# [返り値]
#    なし
#==================================================
sub _init {
	my $self = shift;
	# $self->{'expire'}	= [ sec, usec ];			# 制限時刻
}

#==================================================
# ●インターバル値を指定してタイマをセット
# setinterval( usec )
#
# [引数]
#    usec: インターバル ( 1 = 0.000001秒 )
#
# [返り値]
#    r =
#      0 : (失敗)
#      1 : (成功)
#
# 現在時刻から指定するインターバル後のタイムアウト値をセットします。
# 
# １秒以上のインターバルを設ける場合はこの関数を使用するよりも
# time()を使用する方が適切です。
#==================================================
sub setinterval {
	# printf "\n(%s::setinterval [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	my $usec = shift;								# インターバル

	my $now = [ gettimeofday ];						# 現在時刻(起点時刻)
	my $expire = sumtime( $now, $usec );			# 終点時刻を算出
	if ( defined($expire) ) {
		$self->{'expire'} = $expire;				# 終点時刻を保存
		return 1;
	}
	return 0;
}

#==================================================
# ●タイマをクリア
# clear()
#
# [引数]
#    なし
#
# [返り値]
#    なし
#==================================================
sub clear {
	# printf "\n(%s::clear [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;
	delete( $self->{'expire'} );
}

#==================================================
# ●タイムアウト判定
# timeout()
#
# [引数]
#    なし
#
# [返り値]
#    r =
#       0 : タイムアウトしていない
#       1 : タイムアウトしているまたはタイマが未設定
#==================================================
sub timeout {
	# printf "\n(%s::timeout [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $self = shift;

	return 1 unless ( exists($self->{'expire'}) );						# タイマが設定されてない(1)

	my $r;
	my ( $te_sec, $te_usec ) = @{ $self->{'expire'} };					# 設定値
	my ( $tn_sec, $tn_usec ) = gettimeofday;							# 現在時刻

	# printf "\n** te=[ %d, %d ] ", $te_sec, $te_usec;					# ????
	# printf "\n** tn=[ %d, %d ] ", $tn_sec, $tn_usec;					# ????

	$r = 1 if ( $tn_sec > $te_sec );										# 現在の秒数が設定値を超えている(1)
	$r = 1 if ( ( $tn_sec == $te_sec ) && ( $tn_usec > $te_usec ) );		# 現在の秒数が設定値と同じかつ、
																			# 現在のマイクロ秒が設定値を超えている(1)
	if ($r) {
		delete($self->{'expire'});
		return 1;
	}
	return 0;																# タイムアウトしていない(0)
}

#==================================================
# ●起点時刻に時差を加算した時刻を求める
# r = sumtime( ts, td_usec )
#
# [引数]
#         ts : 起点時刻 ( gettimeofdayの形式 )
#    td_usec : 時差 ( 1 = 0.000001秒 )
#
# [返り値]
#    r = 終点時刻（gettimeofdayの形式）
#==================================================
sub sumtime {
	# printf "\n(%s::sumtime [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $ts = shift;				# 起点時刻（gettimeofday形式）
	my $td_usec = shift;		# 時差（マイクロ秒）
	my $sec  = 0;				# 秒
	my $usec = 1;				# マイクロ秒

	return unless ( defined($ts) );												# 起点時刻が未指定(undef)
	return unless ( $ts->[$sec] =~ /^\d+$/ );									# 起点時刻（秒）が数値でない(undef)		
	return unless ( $ts->[$usec] =~ /^\d+$/ );									# 起点時刻（マイクロ秒）が数値でない(undef)		
	return if ( ( defined($td_usec) == 0 ) || ( $td_usec !~ /^\d+$/ ) );		# 時差が未指定または指定値が数値でない(undef)

	my $td = [];				# 時差（gettimeofday形式）
	my $te;						# 終点時刻（gettimeofday形式）

	# 時差をgettimeofday形式に変換
	$td = ( $td_usec >= 1000000 )
			? [ int( $td_usec / 1000000 ), ( $td_usec % 1000000 ) ]				# １秒以上
			: [ 0, $td_usec ];													# １秒未満

	# printf "\n** ts = [ %d, %d ] ", $ts->[$sec], $ts->[$usec];	# ???? 起点時刻
	# printf "\n** td = [ %d, %d ] ", $td->[$sec], $td->[$usec];	# ???? 時差

	# 時間を合算
	$te->[$sec] = ( $ts->[$sec] + $td->[$sec] );								# 秒の合算
	my $usec_total = $ts->[$usec] + $td->[$usec];								# マイクロ秒の合算
	if ( ( $usec_total ) >= 1000000 ) {											# マイクロ秒の合算値が１秒を超える
		$te->[$sec]++;																# 秒＋１
		$te->[$usec] = ( $usec_total % 1000000 );									# マイクロ秒の合算値を１秒で割った余り
	}
	else {																		# マイクロ秒の合算値が１秒未満
		$te->[$usec] = ( $usec_total );												# マイクロ秒の合算値
	}
	# printf "\n** te = [ %d, %d ]\n", $te->[$sec], $te->[$usec];	# ???? 終点時刻
	return $te;
}

#==================================================
# ●２つの時刻の時間差を返す
# te = difftime( tf, tt )
#
# [引数]
#    tf : 時刻f（gettimeofday形式）
#    tt : 時刻t（gettimeofday形式）
#
# [返り値]
#    te = 時間差（gettimeofdayの形式）
#==================================================
sub difftime {
	# printf "\n(%s::difftime [%s]) ", __PACKAGE__, Debug::_caller(caller) if ($::DEBUG =~ /a/);		# ????
	my $tf = shift;			# 時刻f（gettimeofday形式）
	my $tt = shift;			# 時刻t（gettimeofday形式）

	return unless ( defined($tf) );			# 時刻fが未指定(undef)
	return unless ( defined($tt) );			# 時刻tが未指定(undef)

	my $usec = 1;			# マイクロ秒
	my $sec  = 0;			# 秒
	my $te = [];			# 時間差（gettimeofday形式）

	# printf "\n** tf = [ %d, %d ] ", $tf->[$sec], $tf->[$usec];	# ????
	# printf "\n** tt = [ %d, %d ] ", $tt->[$sec], $tt->[$usec];	# ????
	# print "\n";

	# (1)
	$te->[$sec] = abs( $tt->[$sec] - $tf->[$sec] );
	if ( $tt->[$sec] > $tf->[$sec] ) {
		$tt->[$usec] += ( $te->[$sec] * 1000000 );
	}
	elsif ( $tt->[$sec] < $tf->[$sec] ) {
		$tf->[$usec] += ( $te->[$sec] * 1000000 );
	}

	# printf "\n*(1)* tf = [ %d, %d ] ", $tf->[$sec], $tf->[$usec];	# ????
	# printf "\n*(1)* tt = [ %d, %d ] ", $tt->[$sec], $tt->[$usec];	# ????
	# print "\n";

	# (2)
	my $r = abs( $tt->[$usec] - $tf->[$usec] );
	# printf "\n** r=(%d) ", $r;									# ???? 時差（マイクロ秒）

 	$te->[$sec] = ( $r >= 1000000 ) ? int( $r / 1000000 ) : 0;		# 時間差（秒）
	$te->[$usec] = $r % 1000000;									# 時間差（マイクロ秒）

	# printf "\n*(2)* te = [ %d, %d ] ", $te->[$sec], $te->[$usec];	# ???? 時間差
	return $te;
}

printf("\n(%s) [ %s ] ", $VERSION, __PACKAGE__ ) if ( $::DEBUG =~ /a/ );

#================================================================
=pod
=encoding utf8
=head1 スクリプト名
Timer.pm - BBS.pm サブモジュール 【 タイマ 】
=head1 著者
naoit0 <https://www.naoit0.com/projects/bbs_pm/>
=cut
1;