package Debug;
our $NOLOAD=1;
#===========================================================
# デバッグライブラリ
#
our $VERSION = "----.--------.----";
#===========================================================
use utf8;
use strict;
use warnings;
use Encode qw( encode decode );
use Data::Dumper;
	$Data::Dumper::Sortkeys = 1;			# ハッシュのキーをソートする
#	$Data::Dumper::Indent = 1;				# インデントを縮める
#	$Data::Dumper::Terse = 1;				# $VAR数字要らない

	#【 その他 】
	my $ctrlcode = [										# 制御文字置換テーブル（テスト用）
		'NUL', 'SOH', 'STX', 'ETX', 'EOT', 'ENQ', 'ACK', 'BEL',
		'BS',  'HT',  'LF',  'VT',  'FF',  'CR',  'SO',  'SI',
		'DLE', 'DC1', 'DC2', 'DC3', 'DC4', 'NAK', 'SYN', 'ETB',
		'CAN', 'EM',  'SUB', 'ESC', 'FS',  'GS',  'RS',  'US'
	];

#===========================================================
# ●呼び出し元の情報を返す
# r = _caller();
#
# [引数]
#    なし
#
# [返り値]
#    r = 呼び出し元情報
#===========================================================
sub _caller {
	my $self = shift;
	return sprintf("%s:%s", $_[0], $_[1]) if ( $#_ == 1 );
	return sprintf("pkg=( %s ), src=( %s:%s )", $_[0], $_[1], $_[2]) if ( $#_ == 2 );
}

#===========================================================
# ●文字列のダンプ
# msg = dump( str )
#
# [引数]
#    str : 文字列
#
# [返り値]
#    msg = ダンプ文字列
#===========================================================
sub dump {
	# printf "\n\n(%s::dump [%s]) ", __PACKAGE__, Debug::_caller(caller);		# ????
	my $str = join('',@_);
	my @d;
	map {
		my ($chr, $hex);
		if ( ord($_) > 255 ) {
			$chr = sprintf( "'%s'", $_ );
			$hex = join('', map {
									sprintf "[%02x]", ord($_)
								} split( '',encode('utf8', $_) ) );
		}
		else {
			if (ord($_) <= 31) {
				$chr = sprintf("<%s>", $ctrlcode->[ ord($_) ]);
			}
			elsif (ord($_) >= 128) {
				$chr = "'??'";
			}
			else {
				$chr = sprintf(" '%s'", $_);
			}
			$hex = sprintf("[%02x]", ord($_));
		}
		push(@d, sprintf("\n-- \t%s  %s", $chr, $hex));
	} split(//, $str);
	# print "\n";
	return join("",@d);
}

printf("\n(%s) [ %s ] ", $VERSION, __PACKAGE__ ) if ( $::DEBUG =~ /a/ );

#===========================================================
=pod
=encoding utf8
=head1 スクリプト名
Debug.pm - デバッグライブラリ
=head1 著者
naoit0 <https://www.naoit0.com/>
=cut
1;